CC=gcc
CFLAGS= -Wall -g
LIBS_BASE= -lirctad -lircredes -lpthread -L./lib/ -lsockets
LIBS_CLIENTE= -lircinterface -lircredes -L./lib/ -lsocketsclient -rdynamic
GTKFLAGS= `pkg-config --cflags gtk+-3.0`
GTKLIBS= `pkg-config --libs gtk+-3.0` -lpthread
EXE_SERVIDOR= -o servidor
EXE_CLIENTE= -o cliente
CSERV= src/servidor.c src/acepta_conexion.c src/comandosServidor.c src/analisisComandosServidor.c src/demonizar.c srclib/sockets_servidor.c
CCLIENT= src/funcionesGenericas.c src/analisisComandosCliente.c src/analisisComandosServidor.c src/comandosServidor.c src/comandosCliente.c src/recepcionCliente.c src/chat.c
DIRFLAG = doc includes lib man obj src srclib Makefile autores.txt

servidor: clean libsockets.a compile_servidor empaquetar_servidor run_servidor test_servidor

cliente: clean libsocketsclient.a compile_cliente link_cliente

compile_cliente:
	@echo "\n\t   \033[92mCompilando el cliente IRC...\033[00m"	
	@echo "\t   Creando los .o..."
	@$(CC) -c $(CFLAGS) $(CCLIENT) $(GTKFLAGS) -rdynamic
	@mv *.o ./obj/
	@echo "\t\033[92m   Compilacion realizada                    [OK]\033[00m\n"

link_cliente:
	@echo "\n\t   \033[92mLinkando el cliente IRC...\033[00m"
	@echo "\t   Creando ejecutable..."
	@$(CC) ./obj/*.o $(EXE_CLIENTE) $(LIBS_CLIENTE) $(GTKLIBS)
	@echo "\t\033[92m   Compilacion realizada                    [OK]\033[00m\n"

compile_servidor: help $(CSERV)
	@echo "\n\t   \033[92mCompilando el servidor IRC...\033[00m"	
	@echo "\t   Creando los .o..."
	@$(CC) $(CFLAGS) $(CSERV) $(EXE_SERVIDOR) $(LIBS_BASE)
	@echo "\t\033[92m   Compilacion realizada                    [OK]\033[00m\n"

help:
	@echo "\t   ------------- Instrucciones de uso ---------------"
	@echo "\t   -                                                -"
	@echo "\t   --* all: limpia, compila y ejecuta y muestra     -"
	@echo "\t   --*      el servidor                             -"
	@echo "\t   --*                                              -"
	@echo "\t   --* compile: compila el servidor                 -"
	@echo "\t   --* clean: limpia el proyecto                    -"
	@echo "\t   --* run: ejecuta el servidor                     -"
	@echo "\t   --* test: Busca el servidor en ejecucion         -"
	@echo "\t   --* kill: Para la ejecucion del servidor         -"
	@echo "\t   --* help: Muestra este cuadro de ayuda           -"
	@echo "\t   -                                                -"
	@echo "\t   --------------------------------------------------"


libsockets.a: sockets_servidor.o
	@echo "\t\033[95m Creando librerias		       [OK]\033[00m"
	@ar -rv ./lib/$@ ./obj/$^
	@echo "\t   \033[92mCreacion correcta          [OK]\033[00m\n"

sockets_servidor.o:
	@gcc -c srclib/sockets_servidor.c -o $@
	@mv ./$@ ./obj/

libsocketsclient.a: sockets_cliente.o
	@echo "\t\033[95m Creando librerias		       [OK]\033[00m"
	@ar -rv ./lib/$@ ./obj/$^
	@echo "\t   \033[92mCreacion correcta          [OK]\033[00m\n"

sockets_cliente.o:
	@gcc -c srclib/sockets_cliente.c -o $@ `pkg-config --cflags gtk+-3.0 --libs gtk+-3.0`
	@mv ./$@ ./obj/

clean:
	clear
	@echo "\n\t   Limpiando ficheros .o..."
	@rm -f *.o
	@echo "\t   Limpiando ejecutable..."
	@rm -f *~
	@rm -f servidor
	@rm -f cliente
	@rm -f G-2362-10-P2.tar.gz
	@rm -f ./obj/*.o
	@echo "\t   \033[92mLimpieza de proyecto                    [OK]\033[00m\n"

run_servidor:
	@echo "\n\033[92mEjecutando servidor...\033[00m"	
	./servidor
	@echo "\033[92mServidor levantado                    [OK]\033[00m\n"

test_servidor:
	@echo "\n\033[92mBuscando servidor...\033[00m"	
	ps -e | grep servidor

kill:
	@echo "\n\t   Parando servidor..."
	@pkill "servidor"
	@echo "\t   \033[92mServidor parado                    [OK]\033[00m\n"

empaquetar_servidor:
	@echo "\n\t   Empaquetando proyecto..."
	@rm -rf G-2362-10-P1
	@rm -f G-2362-10-P1.tar.gz
	@mkdir G-2362-10-P1
	@cp -R $(DIRFLAG) G-2362-10-P1
	@tar -czvf G-2362-10-P1.tar.gz G-2362-10-P1
	@rm -rf G-2362-10-P1
	@echo "\t   \033[92mProyecto empaquetado                    [OK]\033[00m\n"

empaquetar_cliente:
	@echo "\n\t   Empaquetando proyecto..."
	@rm -rf G-2362-10-P2
	@rm -f G-2362-10-P2.tar.gz
	@mkdir G-2362-10-P2
	@cp -R $(DIRFLAG) G-2362-10-P2
	@tar -czvf G-2362-10-P2.tar.gz G-2362-10-P2
	@rm -rf G-2362-10-P2
	@echo "\t   \033[92mProyecto empaquetado                    [OK]\033[00m\n"	