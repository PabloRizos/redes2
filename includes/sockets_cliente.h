#ifndef _SOCKETSCLIENTE_H
#define _SOCKETSCLIENTE_H

#include "../includes/definiciones.h"

int abrirSocket(int puerto, char * conexion);
void cerrarSocket(int socket);
int recibir(int socket_id, char * mensaje);
int enviar(int socket_id, char * mensaje);

#endif
