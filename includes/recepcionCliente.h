#ifndef _RECEPCIONCLIENTE_H
#define _RECEPCIONCLIENTE_H

#include "../includes/analisisComandosCliente.h"
#include "../includes/analisisComandosServidor.h"

void manejador (int signum);
void * recibeClienteHilo(void * args);

#endif