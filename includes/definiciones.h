#ifndef _DEFINICIONES_H
#define _DEFINICIONES_H

#include <stdio.h>
#include <redes2/ircxchat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include<arpa/inet.h>


#define PUERTO_IRC 6667
#define ERR -1
#define OK 1
#define MAX_CADENA 8192 /* Tamanio minimo de segmento TCP */
#define ERR_CADENA 256
#define TAM_MAX_MENSAJE 512
#define IP_MAX 120
#define MAX_CONEXIONES 100
#define DOWN_SOCKET 2

pthread_mutex_t mutex;

#endif
