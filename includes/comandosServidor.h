#ifndef _COMANDOSSERVIDOR_H
#define _COMANDOSSERVIDOR_H

#include "../includes/funcionesGenericas.h"

int analizarComandoPass(char * comando);
int analizarComandoNick(char * comando);
int analizarComandoUser(char * comando);
int analizarComandoWelcome(char * comando);
int analizarComandoYourHost(char * command);
int analizarComandoYourHost(char * command);
int analizarComandoBounce(char * command);
int analizarComandoLusersClient(char * comando);
int analizarComandoNameReply(char * comando);
int analizarComandoEndNames(char * comando);
int analizarComandoLusers(char * comando);
int analizarComandoLusersChannels(char * comando);
int analizarComandoEndMotd(char * comando);
int analizarComandoCreated(char * command);
int analizarComandoMyInfo(char * command);
int analizarComandoMotd(char * comando);
int analizarComandoJoin(char * comando);
int analizarComandoList(char * comando);
int analizarComandoWho(char * comando);
int analizarComandoEndOfWho(char * comando);
int analizarComandoWhoIs(char * comando);
int analizarComandoNames(char * comando);
int analizarComandoKick(char * comando);
int analizarComandoPrivMsg(char * comando);
int analizarComandoTopic(char * comando);
int analizarComandoAway(char * comando);
int analizarComandoPart(char * comando);
int analizarComandoPing(char * comando);
int analizarComandoQuit(char * comando);
int analizarComandoMode(char * comando);
int analizarComandoErrCannotSendToChan(char * comando);
int analizarComandoPong(char * comando);
int analizarComandoDesconocido(char * comando);

int analizarComandoERRNickNameInUse(char * comando);

#endif
