#ifndef _COMANDOSCLIENTE_H
#define _COMANDOSCLIENTE_H

#include "../includes/funcionesGenericas.h"

int conectaCliente(int socket_id);
int analizaUNickCliente(char * command);
int analizaUNamesCliente(char * command);
int analizaUJoinCliente(char * command);
int analizaUPartCliente(char * command);
int analizaULeaveCliente(char * command);
int analizaUQuitCliente(char * command);
void analizaGiveOp(char * channel, char *nick);
void analizaTakeOp(char * channel, char * nick);
void cambiarTopic(char * topicdata);
int analizaUMotdCliente(char * command);
int analizaUAwayCliente(char * command);
int analizaUWhoIsCliente(char * command);
int analizaUWhoCliente(char * command);
int analizaUInviteCliente(char * command);
int analizaUNoticeCliente(char * command);
int analizaUKickCliente(char * command);
int analizaUTopicCliente(char * command);
int analizaUListCliente(char * command);
int analizaUMsgCliente(char * command);
int analizaUCycleCliente(char * command);
int analizarTextoUsuario(char * command);


#endif
