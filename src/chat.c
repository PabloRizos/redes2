
#include "../includes/recepcionCliente.h"

extern int socket_id;
pthread_t idHilo = 0;

/**
 * \defgroup IRCInterfaceCallbacks Callbaks del interfaz
 *
 */

/**
 * \addtogroup IRCInterfaceCallbacks
 * Funciones que van a ser llamadas desde el interface y que deben ser implementadas por el usuario.
 * Todas estas funciones pertenecen al hilo del interfaz.
 *
 * El programador puede, por supuesto, separar todas estas funciones en múltiples ficheros a
 * efectos de desarrollo y modularización.
 *
 * <hr>
 */

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateProtectTopic IRCInterface_ActivateProtectTopic
 *
 * @brief Llamada por el botón de activación de la protección de tópico.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateProtectTopic (char * channel)
 *
 * @description
 * Llamada por el botón de activación de la protección de tópico.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/
void IRCInterface_ActivateProtectTopic(char* channel) {
	char* msgPrtctTopic = NULL;

	IRCMsg_Mode(&msgPrtctTopic, NULL, channel, "+t", NULL);
	enviar(socket_id, msgPrtctTopic);
	IRCInterface_PlaneRegisterOutMessage(msgPrtctTopic);
	printf("EXTERNMSG: %s", msgPrtctTopic);
	free(msgPrtctTopic);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateProtectTopic IRCInterface_DeactivateProtectTopic
 *
 * @brief Llamada por el botón de desactivación de la protección de tópico.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateProtectTopic (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación de la protección de tópico.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateProtectTopic(char* channel) {
	char* msgPrtctTopic = NULL;

	IRCMsg_Mode(&msgPrtctTopic, NULL, channel, "-t", NULL);
	enviar(socket_id, msgPrtctTopic);
	IRCInterface_PlaneRegisterOutMessage(msgPrtctTopic);
	printf("EXTERNMSG: %s", msgPrtctTopic);
	free(msgPrtctTopic);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateExternalMessages IRCInterface_ActivateExternalMessages
 *
 * @brief Llamada por el botón de activación de la recepción de mensajes externos.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateExternalMessages (char * channel)
 *
 * @description
 * Llamada por el botón de activación de la recepción de mensajes externos.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivateExternalMessages(char* channel) {
	char* msgExternal = NULL;

	IRCMsg_Mode(&msgExternal, NULL, channel, "+n", NULL);
	enviar(socket_id, msgExternal);
	IRCInterface_PlaneRegisterOutMessage(msgExternal);
	printf("EXTERNMSG: %s", msgExternal);
	free(msgExternal);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateExternalMessages IRCInterface_DeactivateExternalMessages
 *
 * @brief Llamada por el botón de desactivación de la recepción de mensajes externos.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateExternalMessages (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación de la recepción de mensajes externos.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateExternalMessages(char* channel) {
	char* msgExternal = NULL;

	IRCMsg_Mode(&msgExternal, NULL, channel, "-n", NULL);
	enviar(socket_id, msgExternal);
	IRCInterface_PlaneRegisterOutMessage(msgExternal);
	printf("EXTERNMSG: %s", msgExternal);
	free(msgExternal);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateSecret IRCInterface_ActivateSecret
 *
 * @brief Llamada por el botón de activación de canal secreto.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateSecret (char * channel)
 *
 * @description
 * Llamada por el botón de activación de canal secreto.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivateSecret(char* channel) {
	char* msgSecret = NULL;

	IRCMsg_Mode(&msgSecret, NULL, channel, "+s", NULL);
	enviar(socket_id, msgSecret);
	IRCInterface_PlaneRegisterOutMessage(msgSecret);
	printf("SECRETO: %s", msgSecret);
	free(msgSecret);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateSecret IRCInterface_DeactivateSecret
 *
 * @brief Llamada por el botón de desactivación de canal secreto.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateSecret (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación de canal secreto.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateSecret(char* channel) {
	char* msgSecret = NULL;

	IRCMsg_Mode(&msgSecret, NULL, channel, "-s", NULL);
	enviar(socket_id, msgSecret);
	IRCInterface_PlaneRegisterOutMessage(msgSecret);
	printf("SECRETO: %s", msgSecret);
	free(msgSecret);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateInvite IRCInterface_ActivateInvite
 *
 * @brief Llamada por el botón de activación de canal de sólo invitación.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateInvite (char * channel)
 *
 * @description
 * Llamada por el botón de activación de canal de sólo invitación.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivateInvite(char* channel) {
	char* msginvite = NULL;

	IRCMsg_Mode(&msginvite, NULL, channel, "+i", NULL);
	enviar(socket_id, msginvite);
	IRCInterface_PlaneRegisterOutMessage(msginvite);
	printf("INVITE: %s", msginvite);
	free(msginvite);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateInvite IRCInterface_DeactivateInvite
 *
 * @brief Llamada por el botón de desactivación de canal de sólo invitación.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateInvite (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación de canal de sólo invitación.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateInvite(char* channel) {
	char* msginvite = NULL;

	IRCMsg_Mode(&msginvite, NULL, channel, "-i", NULL);
	enviar(socket_id, msginvite);
	IRCInterface_PlaneRegisterOutMessage(msginvite);
	printf("INVITE: %s", msginvite);
	free(msginvite);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivatePrivate IRCInterface_ActivatePrivate
 *
 * @brief Llamada por el botón de activación del modo privado.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivatePrivate (char * channel)
 *
 * @description
 * Llamada por el botón de activación del modo privado.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivatePrivate(char* channel) {

	char* msgPrivate = NULL;

	IRCMsg_Mode(&msgPrivate, NULL, channel, "+p", NULL);
	enviar(socket_id, msgPrivate);
	IRCInterface_PlaneRegisterOutMessage(msgPrivate);
	printf("PRIVATE: %s", msgPrivate);
	free(msgPrivate);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivatePrivate IRCInterface_DeactivatePrivate
 *
 * @brief Llamada por el botón de desactivación del modo privado.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivatePrivate (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación del modo privado.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivatePrivate(char* channel) {
	char* msgPrivate = NULL;

	IRCMsg_Mode(&msgPrivate, NULL, channel, "-p", NULL);
	enviar(socket_id, msgPrivate);
	IRCInterface_PlaneRegisterOutMessage(msgPrivate);
	printf("PRIVATE: %s", msgPrivate);
	free(msgPrivate);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateModerated IRCInterface_ActivateModerated
 *
 * @brief Llamada por el botón de activación de la moderación del canal.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateModerated (char * channel)
 *
 * @description
 * Llamada por el botón de activación de la moderación del canal.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivateModerated(char* channel) {
	char* msgModerated = NULL;

	IRCMsg_Mode(&msgModerated, NULL, channel, "+m", NULL);
	enviar(socket_id, msgModerated);
	IRCInterface_PlaneRegisterOutMessage(msgModerated);
	printf("MODERATED: %s", msgModerated);
	free(msgModerated);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateModerated IRCInterface_DeactivateModerated
 *
 * @brief Llamada por el botón de desactivación  de la moderación del canal.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateModerated (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación  de la moderación del canal.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateModerated(char* channel) {
	char* msgModerated = NULL;

	IRCMsg_Mode(&msgModerated, NULL, channel, "-m", NULL);
	enviar(socket_id, msgModerated);
	IRCInterface_PlaneRegisterOutMessage(msgModerated);
	printf("MODERATED: %s", msgModerated);
	free(msgModerated);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateChannelKey IRCInterface_ActivateChannelKey
 *
 * @brief Llamada por el botón de activación de la clave del canal.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateChannelKey (char * channel, char * key)
 *
 * @description
 * Llamada por el botón de activación de la clave del canal. El segundo parámetro es
 * la clave del canal que se desea poner. Si es NULL deberá impedirse la activación
 * con la función implementada a tal efecto.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivateChannelKey(char* channel, char* key) {
	char* msgKey = NULL;
	if (strlen(key) == 0) {
		IRCInterface_WriteChannel(channel, "*", "No puede establecer una contraseña vacia");
		return;
	}

	IRCMsg_Mode(&msgKey, NULL, channel, "+k", key);
	enviar(socket_id, msgKey);
	IRCInterface_PlaneRegisterOutMessage(msgKey);
	printf("KEY: %s", msgKey);
	free(msgKey);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateChannelKey IRCInterface_DeactivateChannelKey
 *
 * @brief Llamada por el botón de desactivación de la clave del canal.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateChannelKey (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación de la clave del canal.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateChannelKey(char* channel) {
	char* msgKey = NULL;

	IRCMsg_Mode(&msgKey, NULL, channel, "-k", NULL);
	enviar(socket_id, msgKey);
	IRCInterface_PlaneRegisterOutMessage(msgKey);
	printf("KEY: %s", msgKey);
	free(msgKey);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ActivateNicksLimit IRCInterface_ActivateNicksLimit
 *
 * @brief Llamada por el botón de activación del límite de usuarios en el canal.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ActivateNicksLimit (char * channel, int * limit)
 *
 * @description
 * Llamada por el botón de activación del límite de usuarios en el canal. El segundo es el
 * límite de usuarios que se desea poner. Si el valor es 0 se sobrentiende que se desea eliminar
 * este límite.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_ActivateNicksLimit(char* channel, int limit) {
	char* msgLimit = NULL;
	char mensaje[TAM_MAX_MENSAJE];

	mensaje[0] = 0;
	sprintf(mensaje, "%d", limit);
	IRCMsg_Mode(&msgLimit, NULL, channel, "+l", mensaje);
	enviar(socket_id, msgLimit);
	IRCInterface_PlaneRegisterOutMessage(msgLimit);
	printf("LIMIT: %s", msgLimit);
	free(msgLimit);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DeactivateNicksLimit IRCInterface_DeactivateNicksLimit
 *
 * @brief Llamada por el botón de desactivación de la protección de tópico.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DeactivateNicksLimit (char * channel)
 *
 * @description
 * Llamada por el botón de desactivación  del límite de usuarios en el canal.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_DeactivateNicksLimit(char* channel) {
	char* msgLimit = NULL;

	IRCMsg_Mode(&msgLimit, NULL, channel, "-l", NULL);
	enviar(socket_id, msgLimit);
	IRCInterface_PlaneRegisterOutMessage(msgLimit);
	printf("LIMIT: %s", msgLimit);
	free(msgLimit);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_NewTopicEnter IRCInterface_NewTopicEnter
 *
 * @brief Llamada cuando se pulsa la tecla ENTER en el campo de tópico.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_NewTopicEnter (char * topicdata)
 *
 * @description
 * Llamada cuando se pulsa la tecla ENTER en el campo de tópico.
 * Deberá intentarse cambiar el tópico del canal.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_NewTopicEnter(char* topicdata) {
	cambiarTopic(topicdata);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_NewCommandText IRCInterface_NewCommandText
 *
 * @brief Llamada la tecla ENTER en el campo de texto y comandos.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_NewCommandText (char * command)
 *
 * @description
 * Llamada de la tecla ENTER en el campo de texto y comandos. El texto deberá ser
 * enviado y el comando procesado por las funciones de "parseo" de comandos de
 * usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_NewCommandText(char* command) {
	analizarComandosCliente(command);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_GiveOp IRCInterface_GiveOp
 *
 * @brief Llamada por el botón "Op".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_GiveOp (char * channel, char *nick)
 *
 * @description
 * Llamada por el botón "Op". Previamente debe seleccionarse un nick del
 * canal para darle "op" a dicho usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_GiveOp(char* channel, char* nick) {
	analizaGiveOp(channel, nick);

}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_TakeOp IRCInterface_TakeOp
 *
 * @brief Llamada por el botón "Quitar Op".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_TakeOp (char *channel, char *nick)
 *
 * @description
 * Llamada por el botón "Quitar Op". Previamente debe seleccionarse un nick del
 * canal para quitarle "op" a dicho usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_TakeOp(char* channel, char* nick) {
	analizaTakeOp(channel, nick);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_GiveVoice IRCInterface_GiveVoice
 *
 * @brief Llamada por el botón "Dar voz".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_GiveVoice (char * channel, char *nick)
 *
 * @description
 * Llamada por el botón "Dar voz". Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_GiveVoice(char* channel, char* nick) {
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_TakeVoice IRCInterface_TakeVoice
 *
 * @brief Llamada por el botón "Quitar voz".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_TakeVoice (char * channel, char *nick)
 *
 * @description
 * Llamada por el botón "Quitar voz". Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_TakeVoice(char* channel, char* nick) {
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_BanNick IRCInterface_BanNick
 *
 * @brief Llamada por el botón "Banear".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_BanNick (char * channel, char *nick)
 *
 * @description
 * Llamada por el botón "Banear". Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_BanNick(char* channel, char* nick) {
	char* msgBan = NULL, *prefix = NULL, *user = NULL, *real = NULL, *host = NULL;
	char mensaje[TAM_MAX_MENSAJE];

	mensaje[0] = 0;


	IRCInterface_GetNickChannelInfo(nick, channel, &user, &real, &host);
	sprintf(mensaje, "*!*%s@*.%s", user, host);
	IRCMsg_Mode(&msgBan, NULL, channel, "+b", mensaje);
	enviar(socket_id, msgBan);
	IRCInterface_PlaneRegisterOutMessage(msgBan);
	printf("BAN: %s", msgBan);
	multiple_free(5, msgBan, prefix, user, real, host);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_KickNick IRCInterface_KickNick
 *
 * @brief Llamada por el botón "Echar".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_KickNick (char * channel, char *nick)
 *
 * @description
 * Llamada por el botón "Echar". Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * No devuelve ningún valor ni código de error.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

void IRCInterface_KickNick(char* channel, char* nick) {
	char comando[TAM_MAX_MENSAJE];

	sprintf(comando, "/KICK %s %s", nick, channel);
	analizaUKickCliente(comando);
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_SendFile IRCInterface_SendFile
 *
 * @brief Llamada por el botón "Enviar Archivo".
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_SendFile (char * filename, char *nick, char *data, long unsigned int length)
 *
 * @description
 * Llamada por el botón "Enviar Archivo". Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función como todos los demás callbacks bloquea el interface
 * y por tanto es el programador el que debe determinar si crea un nuevo hilo para enviar el archivo o
 * no lo hace.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * Devuelve TRUE si se ha establecido la comunicación y FALSE en caso contrario.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

boolean IRCInterface_SendFile(char* filename, char* nick, char* data, long unsigned int length) {
	return TRUE;
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_StartAudioChat IRCInterface_StartAudioChat
 *
 * @brief Llamada por el botón "Iniciar" del diálogo de chat de voz.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_StartAudioChat (char *nick)
 *
 * @description
 * Llamada por el botón "Iniciar" del diálogo de chat de voz. Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función como todos los demás callbacks bloquea el interface
 * y por tanto para mantener la funcionalidad del chat de voz es imprescindible crear un hilo a efectos
 * de comunicación de voz.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * Devuelve TRUE si se ha establecido la comunicación y FALSE en caso contrario.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

boolean IRCInterface_StartAudioChat(char* nick) {
	return TRUE;
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_StopAudioChat IRCInterface_StopAudioChat
 *
 * @brief Llamada por el botón "Parar" del diálogo de chat de voz.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_StopAudioChat (char *nick)
 *
 * @description
 * Llamada por el botón "Parar" del diálogo de chat de voz. Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función sólo para la comunicación que puede ser reiniciada.
 * Evidentemente tiene que actuar sobre el hilo de chat de voz.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * Devuelve TRUE si se ha parado la comunicación y FALSE en caso contrario.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

boolean IRCInterface_StopAudioChat(char* nick) {
	return TRUE;
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_ExitAudioChat IRCInterface_ExitAudioChat
 *
 * @brief Llamada por el botón "Cancelar" del diálogo de chat de voz.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_ExitAudioChat (char *nick)
 *
 * @description
 * Llamada por el botón "Parar" del diálogo de chat de voz. Previamente debe seleccionarse un nick del
 * canal para darle voz a dicho usuario. Esta función cierrala comunicación. Evidentemente tiene que
 * actuar sobre el hilo de chat de voz.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * Devuelve TRUE si se ha cerrado la comunicación y FALSE en caso contrario.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

boolean IRCInterface_ExitAudioChat(char* nick) {
	return TRUE;
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_DisconnectServer IRCInterface_DisconnectServer
 *
 * @brief Llamada por los distintos botones de desconexión.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_DisconnectServer (char * server, int port)
 *
 * @description
 * Llamada por los distintos botones de desconexión. Debe cerrar la conexión con el servidor.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * Devuelve TRUE si se ha cerrado la conexión y FALSE en caso contrario.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

boolean IRCInterface_DisconnectServer(char* server, int port) {

	pthread_cancel(idHilo);
	cerrarSocket(socket_id);

	return TRUE;
}

/**
 * \ingroup IRCInterfaceCallbacks
 *
 * @page IRCInterface_Connect IRCInterface_Connect
 *
 * @brief Llamada por los distintos botones de conexión.
 *
 * @synopsis
 *	#include <redes2/ircxchat.h>
 *
 * 	void IRCInterface_Connect (char *nick, char * user, char * realname, char * password, char * server, int port, boolean ssl)
 *
 * @description
 * Llamada por los distintos botones de conexión. Si implementará la comunicación completa, incluido
 * el registro del usuario en el servidor.
 *
 * En cualquier caso sólo se puede realizar si el servidor acepta la orden.
 *
 * Esta función debe ser implementada por el alumno.
 *
 * @return
 * Devuelve IRC_OK si todo ha sido correcto, IRCERR_NOSSL
 * si el valor de SSL es TRUE y no se puede activar la conexión SSL pero sí una conexión no protegida.
 * Devolverá IRCERR_NOCONNECT en caso de que no se pueda realizar la comunicación.
 *
 * @author
 * Eloy Anguiano (eloy.anguiano@uam.es)
 *
 *<hr>
*/

long IRCInterface_Connect(char* nick, char* user, char* realname, char* password, char* server, int port, boolean ssl) {
	char cadena_err[ERR_CADENA];
	int socket_id;

	printf("Entro en IRCInterface_Connect\n");
	cadena_err[0] = 0;

	/* Control de parametros de entrada */
	if (nick == NULL || user == NULL || realname == NULL || server == NULL || port < 1) {
		strcpy(cadena_err, "ERROR: Parametros introducidos incorrectos");
		IRCInterface_ErrorDialog(cadena_err);
		IRCInterface_WriteSystem(NULL, cadena_err);
		return IRCERR_NOCONNECT;
	}

	/* Abrimos el socket para administrar la conexion */
	socket_id = abrirSocket(port, server);
	/** Lanzamos el hilo de recepcion de mensajes */
	pthread_create(&idHilo, NULL, (void*)recibeClienteHilo, NULL);
	printf("socket devuelto: %d\n", socket_id);

	/* Enviamos la informacion necesaria con la funcion conectaCliente */
	printf("Llamada a conectaCLiente");
	if (conectaCliente(socket_id) == IRCERR_NOCONNECT) {
		strcpy(cadena_err, "ERROR: No se puede realizar la conexion");
		cerrarSocket(socket_id);
		IRCInterface_ErrorDialog(cadena_err);
		IRCInterface_WriteSystem(NULL, cadena_err);
		return IRCERR_NOCONNECT;
	}

	return IRC_OK;
}


/***************************************************************************************************/
/***************************************************************************************************/
/**                                                                                               **/
/** MMMMMMMMMM               MMMMM           AAAAAAA           IIIIIII NNNNNNNNNN          NNNNNN **/
/**  MMMMMMMMMM             MMMMM            AAAAAAAA           IIIII   NNNNNNNNNN          NNNN  **/
/**   MMMMM MMMM           MM MM            AAAAA   AA           III     NNNNN NNNN          NN   **/
/**   MMMMM  MMMM         MM  MM            AAAAA   AA           III     NNNNN  NNNN         NN   **/
/**   MMMMM   MMMM       MM   MM           AAAAA     AA          III     NNNNN   NNNN        NN   **/
/**   MMMMM    MMMM     MM    MM           AAAAA     AA          III     NNNNN    NNNN       NN   **/
/**   MMMMM     MMMM   MM     MM          AAAAA       AA         III     NNNNN     NNNN      NN   **/
/**   MMMMM      MMMM MM      MM          AAAAAAAAAAAAAA         III     NNNNN      NNNN     NN   **/
/**   MMMMM       MMMMM       MM         AAAAA         AA        III     NNNNN       NNNN    NN   **/
/**   MMMMM        MMM        MM         AAAAA         AA        III     NNNNN        NNNN   NN   **/
/**   MMMMM                   MM        AAAAA           AA       III     NNNNN         NNNN  NN   **/
/**   MMMMM                   MM        AAAAA           AA       III     NNNNN          NNNN NN   **/
/**  MMMMMMM                 MMMM     AAAAAA            AAAA    IIIII   NNNNNN           NNNNNNN  **/
/** MMMMMMMMM               MMMMMM  AAAAAAAA           AAAAAA  IIIIIII NNNNNNN            NNNNNNN **/
/**                                                                                               **/
/***************************************************************************************************/
/***************************************************************************************************/



int main(int argc, char* argv[]) {

	/** TODO Crear hilo de atencion, recibiendo constantemente del servidor */

	/* La función IRCInterface_Run debe ser llamada al final      */
	/* del main y es la que activa el interfaz gráfico quedándose */
	/* en esta función hasta que se pulsa alguna salida del       */
	/* interfaz gráfico.                                          */
	IRCInterface_Run(argc, argv);

	return 0;
}
