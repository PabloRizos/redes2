
#include "../includes/comandosCliente.h"

extern int socket_id;
extern pthread_t idHilo;
int flagJoin = 0;
/**
 * \defgroup ComandosCliente Funciones del cliente
 *
 */
/**
 * \addtogroup ComandosCliente
 * Funciones que van a ser llamadas desde el xchat2 y que serviran para proporcionar
 * la funcionalidad y encapsulacion necesaria para la correcta ejecucion del mismo
 *
 * <hr>
 */
/**
 * @brief Conexion con el servidor
 *
 * @synopsis
 *	#include "comandosCliente.h"
 *
 * int conectaCliente(int socket_id)
 * @description
 * Llamada por el botón de activación de la protección de tópico.
 *
 * Conexion con el servidor usando los comandos NICK y USER
 *
 * @return
 * IRC_OK o IRCERR_NOCONNECTION en caso de error
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int conectaCliente(int socket_id) {

	char* nickcomando = NULL, * usercomando = NULL, *prefix = NULL,  *errmsg = NULL, *mensaje = NULL, *mode = NULL;
	char* nick = NULL, *user = NULL, *realname = NULL, *pass = NULL, *server = NULL;
	int port = 0, ssl = 0;
	long dev = 0;

	//IRC_ComplexUser (&prefix, u->nick, u->user, char *host, char *server);
	/** NICK-USER PIPELINE y Enviar al servidor */
	printf("Antes de IRCInterface_GetMyUserInfo");
	puts("");

	IRCInterface_GetMyUserInfo(&nick, &user, &realname, &pass, &server, &port, &ssl);
	printf("ANTES DE STRLEN NICK\n");
	printf("user: %s\n", user);
	printf("realname: %s\n", realname);
	printf("pass: %s\n", pass);
	printf("server: %s\n", server);
	printf("port: %d\n", port);
	printf("ssl: %d\n", ssl);
	puts("");

	//enviar(socket_id, "CAP LS");
	if (IRCMsg_Nick(&nickcomando, NULL, nick, NULL) != IRC_OK) {
		printf("No se ha introducido nincun nick");
		IRCMsg_ErrNoNickNameGiven(&errmsg, NULL, NULL);
		IRCInterface_WriteSystem(NULL, errmsg);
		return IRC_OK;
	}

	dev = IRCMsg_User(&usercomando, NULL, user, "w", realname);
	switch (dev) {
	case IRCERR_NOUSER:
		printf("No se ha especificado usuario");
		IRCMsg_ErrNeedMoreParams(&errmsg, NULL, nick, "NICK");
		IRCInterface_WriteSystem(NULL, errmsg);
		break;

	case IRCERR_NOMODE:
		printf("No se ha especificado modo");
		IRCMsg_ErrNeedMoreParams(&errmsg, NULL, nick, "NICK");
		IRCInterface_WriteSystem(NULL, errmsg);
		break;

	case IRCERR_NOREALNAME:
		printf("No se ha especificado nombre real");
		IRCMsg_ErrNeedMoreParams(&errmsg, NULL, nick, "NICK");
		IRCInterface_WriteSystem(NULL, errmsg);
		break;
	}

	if (dev != IRC_OK)
		return IRC_OK;

	puts(usercomando);
	if (IRC_PipelineCommands(&mensaje, nickcomando, usercomando, NULL) != IRC_OK) {
		printf("Dentro del pipe\n");
		enviar(socket_id, nickcomando);
		puts(nickcomando);
		enviar(socket_id, usercomando);
		puts(usercomando);
	} else {
		printf("Despues del pipe\n");
		enviar(socket_id, mensaje);
		puts(mensaje);
	}
	multiple_free(11, mensaje, nickcomando, usercomando, errmsg, prefix, nick, user, realname, pass, server, mode);
	return IRC_OK;
}


/**
 * @brief analizaGiveOp
 *
 * @synopsis
 *	#include "comandosCliente.h"
 *
 * void analizaGiveOp(char *channel, char *nick)
 * @description
 * Llamada por el xchat para proporcionar permisos de op
 *
 *
 * @return
 * Devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
void analizaGiveOp(char* channel, char* nick) {

	char* nick1 = NULL, *prefix = NULL, *user = NULL, *realname = NULL, *pass = NULL;
	char* server = NULL, *msg = NULL, *host = NULL;
	int port = 0, ssl = 0;

	printf("GIVEOP\n");
	IRCInterface_GetMyUserInfo(&nick1, &user, &realname, &pass, &server, &port, &ssl);
	IRCMsg_Mode(&msg, prefix, channel, "+o", nick);

	enviar(socket_id, msg);
	IRCInterface_PlaneRegisterOutMessage(msg);
	multiple_free(8, nick1, user, realname, pass, server, msg, host, prefix);
	printf("SALGO DE GIVEOP\n");
}


/**
 * @brief analizaTakeOp
 *
 * @synopsis
 *	#include "comandosCliente.h"
 *
 * void analizaTakeOp(char * channel, char * nick)
 * @description
 * Llamada por el xchat para quitar permisos de op
 *
 *
 * @return
 * Devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
void analizaTakeOp(char* channel, char* nick) {

	char* nick1 = NULL, *prefix = NULL, *user = NULL, *realname = NULL, *pass = NULL;
	char* server = NULL, *msg = NULL, *host = NULL;
	int port = 0, ssl = 0;

	printf("TAKEOP\n");
	IRCInterface_GetMyUserInfo(&nick1, &user, &realname, &pass, &server, &port, &ssl);
	IRCMsg_Mode(&msg, prefix, channel, "-o", nick);

	enviar(socket_id, msg);
	IRCInterface_PlaneRegisterOutMessage(msg);
	multiple_free(8, nick1, user, realname, pass, server, msg, host, prefix);
	printf("SALGO DE TAKEOP\n");
}

/**
 * @brief Creacion de un mensaje de TOPIC
 *
 * @synopsis
 *	#include "comandosCliente.h"
 *
 * void cambiarTopic(char * topicdata)
 * @description
 * Llamada por la funcion de cambiarTopci
 *
 * Envio de mensaje de topic con el topic especificado al servidor
 *
 * @return
 * Devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
void cambiarTopic(char* topicdata) {

	char* topicMsg = NULL;

	IRCMsg_Topic(&topicMsg, NULL, IRCInterface_ActiveChannelName(), topicdata);
	enviar(socket_id, topicMsg);
}
/**
 * @brief Creacion de un JOIN
 *
 * @synopsis
 *	#include "comandosCliente.h"
 *
 * int analizaUJoinCliente(char * command)
 * @description
 * Llamada por la funcion de analizarComandosCliente
 *
 * Conexion con el canal adecuado
 *
 * @return
 * Devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUJoinCliente(char* command) {

	char* channel = NULL, * password = NULL, * msgJoin = NULL;
	printf("\nEntro en el case UJOIN\n");
	// long IRCUserParse_Join( char *in, char **channel, char **password)
	IRCUserParse_Join(command, &channel, &password);
	printf("Canal: %s, Password: %s\n", channel, password);
	if (channel == NULL) {
		IRCInterface_WriteSystem(NULL, "Debe introducir un canal como argumento\n");
		printf("ERROR: Salgo del case UJOIN\n");
		return ERR;
	}

	flagJoin = 1;
	// long IRCMsg_Join (char **command, char *prefix, char * channel, char *key, char *msg)
	IRCMsg_Join(&msgJoin, NULL, channel, password, NULL);
	printf("MsgJoin: %s\n", msgJoin);
	enviar(socket_id, msgJoin);
	printf("MsgJoin enviado al servidor\n");
	// void IRCInterface_PlaneRegisterOutMessage (char * plainMsg)
	IRCInterface_PlaneRegisterOutMessage(msgJoin);
	// IRCInterface_AddNewChannel (char * channel, long mode)
	////////// IRCInterface_AddNewChannel(channel, 0);
	multiple_free(3, channel, msgJoin, password);
	printf("Salgo del case UJOIN\n");
	return OK;
}

/**
 * @brief analizaUNickCliente
 *
 * @synopsis
 *
 * int analizaUNickCliente(char * command)
 * @description
 * Funcion que analiza la solicitud del cambio de nick por el cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUNickCliente(char* command) {
	char* nick = NULL, * msgNick = NULL;
	printf("\nEntro en el case UNICK\n");
	// long IRCUserParse_Nick (char *strin, char **newnick)
	IRCUserParse_Nick(command, &nick);
	printf("Nick nuevo: %s\n", nick);
	if (nick == NULL) {
		IRCInterface_WriteSystem(NULL, "(UNICK) Debe introducir un nick como argumento\n");
		printf("ERROR: Salgo del case UNICK\n");
		return ERR;
	}
	// long IRCMsg_Nick (char **command, char *prefix, char * nick, char *msg)
	IRCMsg_Nick(&msgNick, NULL, nick, NULL);
	printf("MsgNick: %s\n", msgNick);
	enviar(socket_id, msgNick);
	printf("MsgNick enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgNick);

	multiple_free(2, nick, msgNick);
	printf("Salgo del case UNICK\n");
	return OK;
}

/**
 * @brief analizaUNamesCliente
 *
 * @synopsis
 *
 * int analizaUNamesCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UNames por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUNamesCliente(char* command) {
	char* channelNames = NULL, * targetNames = NULL, * msgNames = NULL, *activeChannel = NULL;
	printf("\nEntro en el case UNAMES\n");
	// long IRCUserParse_Names( char *in, char **channel, char **targetserver)
	IRCUserParse_Names(command, &channelNames, &targetNames);
	printf("ChannelNames: %s , TargetNames: %s\n", channelNames, targetNames);
	// long IRCMsg_Names (char **command, char *prefix, char * channel, char *target)
	if (channelNames == NULL) {
		activeChannel = IRCInterface_ActiveChannelName();
		IRCMsg_Names(&msgNames, NULL, activeChannel, targetNames);
	} else {
		IRCMsg_Names(&msgNames, NULL, channelNames, targetNames);
	}

	printf("MsgNames: %s\n", msgNames);
	enviar(socket_id, msgNames);

	printf("MsgNames enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgNames);

	multiple_free(3, channelNames, targetNames, msgNames);
	printf("Salgo del case UNAMES\n");
	return OK;
}

/**
 * @brief analizaUPartCliente
 *
 * @synopsis
 *
 * int analizaUPartCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UPART por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUPartCliente(char* command) {
	char* channelPart = NULL, * msgPart = NULL, *actualChannel = NULL;
	char mensaje[TAM_MAX_MENSAJE];

	mensaje[0]=0;

	printf("\nEntro en el case UPART\n");

	IRCUserParse_Part(command, &channelPart);
	printf("ChannelPart: %s\n", channelPart);

	if (channelPart == NULL) {
		actualChannel = IRCInterface_ActiveChannelName();
		if (!strcmp(actualChannel, "System")) {
			IRCInterface_WriteSystem(NULL, "(UPART) Debe introducir un canal como argumento\n");
			printf("ERROR: Salgo del case UPART\n");
			return ERR;
		}
		IRCMsg_Part(&msgPart, NULL, actualChannel, NULL);
		strcpy(mensaje, actualChannel);
		IRCInterface_RemoveChannel(mensaje);
	} else {

		IRCMsg_Part(&msgPart, NULL, channelPart, NULL);
		IRCInterface_RemoveChannel(channelPart);
	}
	printf("MsgPart: %s\n", msgPart);
	enviar(socket_id, msgPart);
	IRCInterface_PlaneRegisterOutMessage(msgPart);
	multiple_free(2, channelPart, msgPart);
	printf("Salgo del case UPART\n");
	return OK;
}

/**
 * @brief analizaUWhoCliente
 *
 * @synopsis
 *
 * int analizaUWhoCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UWHO por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUWhoCliente(char* command) {

	char* who = NULL, * msgWho = NULL;
	printf("\nEntro en el case UWHO\n");
	// long IRCUserParse_Whois (char *strin, char **command)
	IRCUserParse_Who(command, &who);
	printf("Who: %s\n", who);
	IRCMsg_Who(&msgWho, NULL, who, NULL);
	printf("MsgWho: %s\n", msgWho);
	enviar(socket_id, msgWho);
	printf("MsgWho enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgWho);
	multiple_free(2, who, msgWho);
	printf("Salgo del case UWHO\n");
	return OK;

}
/**
 * @brief analizaULeaveCliente
 *
 * @synopsis
 *
 * int analizaULeaveCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando ULEAVE por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaULeaveCliente(char* command) {

	char* channelLeave = NULL, * msgLeave = NULL;
	printf("\nEntro en el case ULEAVE\n");
	// long IRCUserParse_Leave (char *strin, char **msg)
	IRCUserParse_Leave(command, &channelLeave);
	printf("ChannelLeave: %s\n", channelLeave);
	// long IRCMsg_Part (char **command, char *prefix, char * channel, char *msg)
	if (channelLeave == NULL) {
		IRCInterface_WriteSystem(NULL, "(ULEAVE) Debe introducir un canal como argumento\n");
		printf("ERROR: Salgo del case ULEAVE\n");
		return ERR;
	} else {
		IRCMsg_Part(&msgLeave, NULL, channelLeave, NULL);
		printf("Channels msgLeave: %s\n", msgLeave);
	}
	enviar(socket_id, msgLeave);
	printf("MsgLeave enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgLeave);
	multiple_free(2, channelLeave, msgLeave);
	printf("Salgo del case ULEAVE\n");
	return OK;
}

/**
 * @brief analizaUQuitCliente
 *
 * @synopsis
 *
 * int analizaUQuitCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UQUIT por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUQuitCliente(char* command) {

	char* reasonQuit = NULL, * msgQuit = NULL;

	IRCUserParse_Quit(command, &reasonQuit);

	if (reasonQuit == NULL) {
		printf("No reasonQuit\n");
		IRCMsg_Quit(&msgQuit, NULL, "El usuario se ha desconectado\n");
		cerrarSocket(socket_id);
		pthread_cancel(idHilo);
		exit(1);

	} else {
		printf("ReasonQuit: %s\n", reasonQuit);
		IRCMsg_Quit(&msgQuit, NULL, reasonQuit);
		cerrarSocket(socket_id);
		pthread_cancel(idHilo);
		exit(1);
	}
	// long IRCMsg_Quit (char **command, char *prefix, char * msg)

	printf("MsgQuit: %s\n", msgQuit);
	enviar(socket_id, msgQuit);
	printf("MsgQuit enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgQuit);
	multiple_free(2, reasonQuit, msgQuit);
	printf("Salgo del case UQUIT\n");
	return OK;
}

/**
 * @brief analizaUMotdCliente
 *
 * @synopsis
 *
 * int analizaUMotdCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UMOTD por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUMotdCliente(char* command) {

	printf("\nEntro en el case UMOTD\n");
	char* serverMotd = NULL, * msgMotd = NULL;
	// long IRCUserParse_Motd (char *strin, char **server)
	IRCUserParse_Motd(command, &serverMotd);
	// long IRCMsg_Motd (char **command, char *prefix, char * target)
	if (serverMotd == NULL) {
		printf("No serverMotd\n");
		IRCMsg_Motd(&msgMotd, NULL, NULL);
	} else {
		printf("ServerMotd: %s\n", serverMotd);
		IRCMsg_Motd(&msgMotd, NULL, serverMotd);
	}
	enviar(socket_id, msgMotd);
	printf("MsgMotd enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgMotd);
	multiple_free(2, serverMotd, msgMotd);
	printf("Salgo del case UMOTD\n");
	return OK;
}

/**
 * @brief analizaUAwayCliente
 *
 * @synopsis
 *
 * int analizaUAwayCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UAWAY por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUAwayCliente(char* command) {

	char* reasonAway = NULL, * msgAway = NULL;
	printf("\nEntro en el case UAWAY\n");
	// long IRCUserParse_Away (char *strin, char **reason)
	IRCUserParse_Away(command, &reasonAway);
	if (reasonAway == NULL) {
		printf("ReasonAway: %s\n", reasonAway);
		IRCMsg_Away(&msgAway, NULL, "Away");
	} else {
		IRCMsg_Away(&msgAway, NULL, reasonAway);
		printf("ReasonAway: %s\n", reasonAway);
	}
	printf("MsgAway: %s\n", msgAway);
	enviar(socket_id, msgAway);
	printf("MsgAway enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgAway);
	multiple_free(2, reasonAway, msgAway);
	printf("Salgo del case UAWAY\n");
	return OK;
}

/**
 * @brief analizaUWhoISCliente
 *
 * @synopsis
 *
 * int analizaUWhoIsCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UWHOIS por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUWhoIsCliente(char* command) {
	/******************

	SEGFAULT ???????????

	*******************/
	char* whoIs = NULL, * msgWhoIs = NULL;
	printf("\nEntro en el case UWHOIS\n");
	// long IRCUserParse_Whois (char *strin, char **command)
	// long IRCMsg_Whois (char **command, char *prefix, char * target, char *maskarray)
	IRCUserParse_Whois(command, &whoIs);
	printf("WhoIs: %s\n", whoIs);
	if (whoIs == NULL) {
		IRCInterface_WriteSystem(NULL, "(UWHOIS) Debe introducir un usuario como argumento\n");
		printf("ERROR: Salgo del case UWHOIS\n");
		return ERR;
	} else {
		IRCMsg_Whois(&msgWhoIs, NULL, whoIs, NULL); //target null?????
	}
	printf("MsgWhoIs: %s\n", msgWhoIs);
	enviar(socket_id, msgWhoIs);
	printf("MsgWhoIs enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgWhoIs);
	multiple_free(2, whoIs, msgWhoIs);
	printf("Salgo del case UWHOIS\n");
	return OK;
}
/**
 * @brief analizaUInviteCliente
 *
 * @synopsis
 *
 * int analizaUInviteCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UINVITE por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUInviteCliente(char* command) {

	char* nickInvite = NULL, * channelInvite = NULL, * msgInvite = NULL;

	printf("\nEntro en el case UINVITE\n");
	// long IRCUserParse_Invite (char *strin, char **nick, char **channel)
	IRCUserParse_Invite(command, &nickInvite, &channelInvite);
	printf("NickInvite: %s\n", nickInvite);
	printf("ChannelInvite: %s\n", channelInvite);
	if (channelInvite == NULL || nickInvite == NULL) {
		IRCInterface_WriteSystem(NULL, "(UINVITE) Debe introducir un usuario y un canal como argumento\n");
		printf("ERROR: Salgo del case UINVITE\n");
		return ERR;
	} else {
		// No hay restricciones sobre el nick o el canal segun el RFC
		// long IRCMsg_Invite (char **command, char *prefix, char * nick, char *channel)
		IRCMsg_Invite(&msgInvite, NULL, nickInvite, channelInvite);
	}
	printf("MsgInvite: %s\n", msgInvite);
	enviar(socket_id, msgInvite);
	printf("MsgInvite enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgInvite);
	multiple_free(3, nickInvite, channelInvite, msgInvite);
	printf("Salgo del case UINVITE\n");
	return OK;
}

/**
 * @brief analizaUKickCliente
 *
 * @synopsis
 *
 * int analizaUKickCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UKICK por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUKickCliente(char* command) {

	char* nickKick = NULL, * channelKick = NULL, * msgKick = NULL, *actualChannel = NULL;

	printf("\nEntro en el case UKICK\n");
	// long IRCUserParse_Kick (char *strin, char **nick, char **msg)
	IRCUserParse_Kick(command, &nickKick, &channelKick);
	printf("NickKick: %s, ChannelKick: %s\n", nickKick, channelKick);
	if (nickKick == NULL) {
		IRCInterface_WriteSystem(NULL, "(UKICK) Debe introducir al menos un usuario como argumento\n");
		printf("ERROR: Salgo del case UKICK\n");
		multiple_free(2, nickKick, channelKick);
		return ERR;
	} else {
		if (channelKick == NULL) {
			actualChannel = IRCInterface_ActiveChannelName();
			if (strcmp(actualChannel, "System"))
				IRCMsg_Kick(&msgKick, NULL, actualChannel, nickKick, NULL);

		}
		IRCMsg_Kick(&msgKick, NULL, channelKick, nickKick, NULL);
		printf("MsgKick: %s\n", msgKick);
	}

	enviar(socket_id, msgKick);
	printf("MsgKick enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgKick);
	multiple_free(3, nickKick, channelKick, msgKick);
	printf("Salgo del case UKICK\n");
	return OK;
}
/**
 * @brief analizaUTopicCliente
 *
 * @synopsis
 *
 * int analizaUTopicCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UTOPIC por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUTopicCliente(char* command) {
	char* topicChannel = NULL, * msgTopic = NULL;
	printf("\nEntro en el case UTOPIC\n");
	// long IRCUserParse_Topic (char *strin, char **topic)
	IRCUserParse_Topic(command, &topicChannel);
	printf("TopicChannel: %s\n", topicChannel);
	// long IRCMsg_Topic (char **command, char *prefix, char * channel, char *topic)
	if (topicChannel == NULL) {
		IRCInterface_WriteSystem(NULL, "(UTOPIC) Debe introducir un canal como argumento\n");
		printf("ERROR: Salgo del case UTOPIC\n");
		return ERR;
	}
	IRCMsg_Topic(&msgTopic, NULL, topicChannel, NULL);
	printf("MsgTopic: %s\n", msgTopic);
	enviar(socket_id, msgTopic);
	printf("MsgTopic enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgTopic);
	multiple_free(2, topicChannel, msgTopic);
	printf("Salgo del case UTOPIC\n");
	return OK;
}

/**
 * @brief analizaUListCliente
 *
 * @synopsis
 *
 * int analizaUListCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando ULIST por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUListCliente(char* command) {
	char* channelList = NULL, * searchString = NULL, * msgList = NULL;
	printf("\nEntro en el case ULIST\n");
	// long IRCUserParse_List (char *strin, char **channel, char **searchstring)
	IRCUserParse_List(command, &channelList, &searchString);
	printf("ChannelList: %s, SearchString: %s\n", channelList, searchString);
	// long IRCMsg_List (char **command, char *prefix, char * channel, char *target)
	IRCMsg_List(&msgList, NULL, channelList, NULL);
	printf("MsgList: %s\n", msgList);
	enviar(socket_id, msgList);
	printf("MsgList enviado al servidor\n");
	IRCInterface_PlaneRegisterOutMessage(msgList);
	/***************??????????????????????????????***********************/
	multiple_free(3, channelList, searchString, msgList);
	printf("Salgo del case ULIST\n");
	return OK;
}

/**
 * @brief analizaUListCliente
 *
 * @synopsis
 *
 * int analizaUListCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando ULIST por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUMsgCliente(char* command) {

	char* nickorchannel = NULL, *msg = NULL, *mensaje = NULL, *prefix = NULL, *nick = NULL;
	char* user = NULL, *realname = NULL, *pass = NULL, *server = NULL;
	int port = 0, ssl = 0;

	IRCInterface_GetMyUserInfo(&nick, &user, &realname, &pass, &server, &port, &ssl);

	IRCUserParse_Msg(command, &nickorchannel, &msg);
	IRCMsg_Privmsg(&mensaje, prefix, nickorchannel, msg);
	IRCInterface_PlaneRegisterOutMessage(mensaje);
	if (IRCInterface_QueryChannelExist(nickorchannel))
		IRCInterface_WriteChannel(nickorchannel, nick, msg);
	enviar(socket_id, mensaje);

	multiple_free(9, nickorchannel, msg, mensaje, prefix, nick, user, realname, pass, server);

	return OK;
}
/**
 * @brief analizaUNoticeCliente
 *
 * @synopsis
 *
 * int analizaUNoticeCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UNOTICE por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUNoticeCliente(char* command) {

	char* nickname = NULL, * text = NULL, * msgNotice = NULL;
	printf("\nEntro en el case UNOTICE\n");
	// long IRCUserParse_Notice (char *strin, char **msg)
	IRCUserParse_Notice(command, &nickname, &text);
	printf("ChannelNotice: %s, MsgNotice: %s\n", nickname, text);

	if (nickname == NULL || text == NULL) {
		IRCInterface_WriteSystem(NULL, "(UNOTICE) Debe introducir un nickname y un texto\n");
		printf("ERROR: Salgo del case UNOTICE\n");
		return ERR;
	}
	// long IRCMsg_Notice (char **command, char *prefix, char * msgtarget, char *msg)
	IRCMsg_Notice(&msgNotice, NULL, nickname, text);
	printf("MsgNotice: %s\n", msgNotice);
	enviar(socket_id, msgNotice);
	IRCInterface_PlaneRegisterOutMessage(msgNotice);
	return OK;
}

/**
 * @brief analizaUCycleCliente
 *
 * @synopsis
 *
 * int analizaUCycleCliente(char * command)
 *
 * @description
 * Funcion que analiza el comando UCYCLE por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizaUCycleCliente(char* command) {

	char** channelarray = NULL;
	char* msgPart = NULL, *msgJoin = NULL;
	int numchannels = 0, i = 0;
	// long IRCUserParse_Cycle (char *strin, char ***channelarray, int *numchannels)
	IRCUserParse_Cycle(command, &channelarray, &numchannels);
	printf("NumChannels: %d\n", numchannels);
	for (i = 0; i < numchannels; i++) {
		printf("Channel: %s\n", channelarray[i]);
		/* PART */
		IRCMsg_Part(&msgPart, NULL, channelarray[i], NULL);
		printf("MsgPart: %s\n", msgPart);
		enviar(socket_id, msgPart);
		printf("MsgPart enviado al servidor\n");
		IRCInterface_PlaneRegisterOutMessage(msgPart);
		/* JOIN */
		IRCMsg_Join(&msgJoin, NULL, channelarray[i], NULL, NULL);
		printf("MsgJoin: %s\n", msgJoin);
		enviar(socket_id, msgJoin);
		printf("MsgJoin enviado al servidor\n");
		IRCInterface_PlaneRegisterOutMessage(msgJoin);
	}

	return OK;
}

/**
 * @brief analizaTextoUsuario
 *
 * @synopsis
 *
 * int analizarTextoUsuario(char * command)
 *
 * @description
 * Funcion que analiza el comando TEXTOUSUARIO por parte del cliente
 *
 * @return
 * OK o ERR dependiendo del flujo de la funcion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarTextoUsuario(char* command) {

	char* mensaje = NULL, *prefix = NULL, *nick = NULL;
	char* user = NULL, *realname = NULL, *pass = NULL, *server = NULL;
	int port = 0, ssl = 0;

	IRCInterface_GetMyUserInfo(&nick, &user, &realname, &pass, &server, &port, &ssl);
	printf("ANALIZAR TEXTO USUARIO");
	if (IRCInterface_ActiveChannelName() != NULL) {
		IRCInterface_WriteChannel(IRCInterface_ActiveChannelName(), nick, command);
		IRCMsg_Privmsg(&mensaje, NULL, IRCInterface_ActiveChannelName(), command);

		enviar(socket_id, mensaje);
	}
	multiple_free(7, mensaje, user, realname, pass, server, prefix, nick);
	return OK;
}

