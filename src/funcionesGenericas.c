#include "../includes/funcionesGenericas.h"

/**
 * \defgroup FuncionesGenericas Funciones utiles para la ejecucion del cliente
 *
 */
/**
 * \addtogroup FuncionesGenericas
 * Funciones que sirven de utilidad para la correcta ejecucion y optimizacón 
 * del proyecto
 * <hr>
 */


/**
 * @brief Multiple liberacion de memoria
 *
 * @synopsis
 *
 * void multiple_free(int numparam, ...)
 * @description 
 * 
 * Funcion que libera multiples argumentos a traves de sus parametros variables
 *
 * @return
 * Esta funcion no devuelve nada
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
void multiple_free(int numparam, ...)
{
	int i=0;
    va_list args;
    void *vp;

    va_start(args, numparam);
    for (i=0; i<numparam; i++){
        vp = va_arg(args, void *);
        free(vp);
    }
    va_end(args);
}

/**
 * @brief Hostname To IP
 *
 * @synopsis
 *
 * int hostname_to_ip(char * hostname , char* ip)
 * @description 
 * 
 * Funciones que procesan en profundidad los comandos recibidos desde el servidor
 *
 * @return
 * Esta funcion devuelve un entero significativo si ha salido todo bien o ha
 * habido un error
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int hostname_to_ip(char * hostname , char* ip)
{
    struct hostent *he;
    struct in_addr **addr_list;
    int i;
         
	do{
	    he=gethostbyname(hostname);
    }while(he==NULL);
 
    addr_list = (struct in_addr **) he->h_addr_list;
     
    for(i = 0; addr_list[i] != NULL; i++) 
    {
        //Return the first one;
        strcpy(ip , inet_ntoa(*addr_list[i]));
        return 0;
    }
     
    return 1;
}
