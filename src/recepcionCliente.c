#include "../includes/recepcionCliente.h"

extern int socket_id;
extern pthread_t idHilo;
clock_t tiempo_ping=0;
/** 
 * \defgroup AtencionHilo Funciones de atencion al hilo del cliente
 *
 */
/** 
 * \addtogroup AtencionHilo
 * Funciones que van a ser llamadas cuando se crea el hilo de recepcion del
 * cliente.
 *
 * <hr>
 */


 /**
 * @brief manejador
 *
 * @synopsis
 *
 * void manejador (int signum)
 * @description 
 * 
 * Funcion que se encarga de controlar el protocolo PING
 *
 * @return
 * Esta funcion no devuelve nada
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
void manejador (int signum){

	char * ping=NULL, *prefix=NULL, *server=NULL, *server2=NULL;
  	char * nick=NULL, *user=NULL, *realname=NULL, *pass=NULL;
  	int port=0, ssl=0;
	double secs=0;
  	clock_t t_fin=0;

  	if(tiempo_ping!=0){
    
		t_fin = clock();
   		secs = (double)(t_fin - tiempo_ping) / CLOCKS_PER_SEC;
   		if(secs > 30){

   			cerrarSocket(socket_id);
   			pthread_cancel(idHilo);
   		}
   		else
   			tiempo_ping+=t_fin - tiempo_ping;
   	}
   	else{

	  	tiempo_ping = clock();

	    IRCInterface_GetMyUserInfoThread(&nick, &user, &realname, &pass, &server, &port, &ssl);

	  	IRCMsg_Ping (&ping, NULL, server, NULL);
	  	enviar(socket_id, ping);
  	}
  	multiple_free(8, ping, prefix, server, server2, nick, user, realname, pass);
	alarm(15);

}

/**
 * @brief Atencion Cliente Hilo
 *
 * @synopsis
 *
 * void * atencionClienteHilo()
 * @description 
 * 
 * Funcion que comienza a recibir los mensajes del servidor y los procesa
 *
 * @return
 * Esta funcion devuelve un puntero a void cuando sale del hilo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/

void * recibeClienteHilo(void * args){
	
	char * dev_unpipe=NULL, *command=NULL, mensaje[MAX_CADENA];
	mensaje[0]=0;


	signal(SIGALRM, manejador);
	alarm(15);

	/** Bucle de recepcion de mensajes del servidor */
	while(1) {
		
		dev_unpipe=NULL;
		bzero(mensaje, strlen(mensaje));
		if ((recibir(socket_id, mensaje)) < 0) {
				continue;
		} else {
			if (strcmp(mensaje, "") == 0) {
				// NOTHING
			} else {
				printf("He recibido algo");
				IRCInterface_PlaneRegisterInMessageThread (mensaje);
				/* Comprobamos que el mensaje no este pipeado */
				dev_unpipe = IRC_UnPipelineCommands(mensaje, &command, NULL);
				if (dev_unpipe != NULL) {
					analizarComandosServidor(command);
					free(command);
					while((dev_unpipe=IRC_UnPipelineCommands(NULL, &command, dev_unpipe)) != NULL) {
						analizarComandosServidor(command);
						free(command);
					}
					free(dev_unpipe);

					//Si dev==NULL && command=NULL -> No queda nada
					//Si dev=NULL y command!=NULL -> No queda nada pero comand incompleto
					//Si d!= NULL -> No acabado
				} else {
				//	syslog(LOG_INFO, "Devolucion de IRC_UnPipelineCommands: %s", dev_unpipe);
				}
			}
			
		}
	}



}

