#include "../includes/analisisComandosServidor.h"

/**
 * \defgroup AnalisisComandosServidor Funciones de parseo de mensajes del servidor
 *
 */
/**
 * \addtogroup AnalisisComandosServidor
 * Funciones que van a ser llamadas para parsear y manejar los comandos devueltos
 * por el servidor
 *
 * <hr>
 */


/**
 * @brief Analizar Comando Servidor
 *
 * @synopsis
 *
 * int analizarComandoServidor(char * comando)
 * @description
 *
 * Funcion que analiza un comando recibido desde el servidor
 *
 * @return
 * Esta funcion devuelve un entero que representa un error o una correcta ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandosServidor(char* comando) {

	if (comando == NULL)
		return ERR;

	switch (IRC_CommandQuery(comando)) {

	case PASS:

		if (analizarComandoPass(comando) == ERR) {
			printf("----------Error al ejecutar el comando PASS\n");
		} else {
			printf(" Ejecucion con exito del comando PASS\n");
		}
		break;

	case NICK:
		printf(" Entro en el case NICK\n");

		if (analizarComandoNick(comando) == ERR)
			printf(" Error al ejecutar el comando NICK\n");
		else
			printf(" Ejecucion con exito del comando NICK\n");
		break;

	case USER:
		printf(" Entro en el case USER\n");

		if (analizarComandoUser(comando) == ERR)
			printf(" Error al ejecutar el comando USER\n");
		else
			printf(" Ejecucion con exito del comando USER\n");
		break;


	/*******************************************************************************/
	/*					INICIO DE PARSEO DE MENSAJES DE RESPUESTA 				   */
	/*******************************************************************************/


	case RPL_WELCOME:
		printf(" Entro en el case RPL_WELCOME\n");

		if (analizarComandoWelcome(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_WELCOME\n");
		else
			printf(" Ejecucion con exito del comando RPL_WELCOME\n");
		break;

	case RPL_MOTD:
		printf(" Entro en el case RPL_MOTD\n");

		if (analizarComandoMotd(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_MOTD\n");
		else
			printf(" Ejecucion con exito del comando RPL_MOTD\n");
		break;
	case RPL_YOURHOST:
		printf(" Entro en el case RPL_YOURHOST\n");

		if (analizarComandoYourHost(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_YOURHOST\n");
		else
			printf(" Ejecucion con exito del comando RPL_YOURHOST\n");
		break;
	case RPL_CREATED:
		printf(" Entro en el case RPL_CREATED\n");

		if (analizarComandoCreated(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_CREATED\n");
		else
			printf(" Ejecucion con exito del comando RPL_CREATED\n");
		break;
	case RPL_MYINFO:
		printf(" Entro en el case RPL_MYINFO\n");

		if (analizarComandoMyInfo(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_MYINFO\n");
		else
			printf(" Ejecucion con exito del comando RPL_MYINFO\n");
		break;
	case RPL_BOUNCE:
		printf(" Entro en el case RPL_BOUNCE\n");

		if (analizarComandoBounce(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_BOUNCE\n");
		else
			printf(" Ejecucion con exito del comando RPL_BOUNCE\n");
		break;

	case RPL_LUSERCLIENT:
		printf(" Entro en el case RPL_LUSERCLIENT.\n");

		if (analizarComandoLusersClient(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_LUSERCLIENT.\n");
		else
			printf(" Ejecucion con exito del comando RPL_LUSERCLIENT.\n");
		break;
	case RPL_LUSERME:
		printf(" Entro en el case RPL_LUSERME.\n");

		if (analizarComandoLusers(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_LUSERME.\n");
		else
			printf(" Ejecucion con exito del comando RPL_LUSERME.\n");
		break;

	case RPL_LUSERCHANNELS:
		printf(" Entro en el case RPL_LUSERCHANNELS.\n");

		if (analizarComandoLusersChannels(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_LUSERCHANNELS.\n");
		else
			printf(" Ejecucion con exito del comando RPL_LUSERCHANNELS.\n");
		break;
	case RPL_ENDOFMOTD:
		printf(" Entro en el case RPL_ENDOFMOTD.\n");

		if (analizarComandoEndMotd(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_ENDOFMOTD.\n");
		else
			printf(" Ejecucion con exito del comando RPL_ENDOFMOTD.\n");
		break;
	case ERR_NICKNAMEINUSE:
		printf(" Entro en el case ERR_NICKNAMEINUSE.\n");
		analizarComandoERRNickNameInUse(comando);
		break;

	case RPL_NAMREPLY:
		printf(" Entro en el case RPL_NAMREPLY.\n");

		if (analizarComandoNameReply(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_NAMREPLY.\n");
		else
			printf(" Ejecucion con exito del comando RPL_NAMREPLY.\n");
		break;

	case RPL_ENDOFNAMES:
		printf(" Entro en el case RPL_ENDOFNAMES.\n");

		if (analizarComandoEndNames(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_ENDOFNAMES.\n");
		else
			printf(" Ejecucion con exito del comando RPL_ENDOFNAMES.\n");
		break;
	case RPL_WHOREPLY:
		printf(" Entro en el case RPL_WHOREPLY.\n");

		if (analizarComandoWho(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_WHOREPLY.\n");
		else
			printf(" Ejecucion con exito del comando RPL_WHOREPLY.\n");
		break;

	case RPL_ENDOFWHO:
		printf(" Entro en el case RPL_ENDOFWHO.\n");

		if (analizarComandoEndOfWho(comando) == ERR)
			printf(" Error al ejecutar el comando RPL_ENDOFWHO.\n");
		else
			printf(" Ejecucion con exito del comando RPL_ENDOFWHO.\n");
		break;

	case PONG:
		printf(" Entro en el case PONG.\n");

		if (analizarComandoPong(comando) == ERR)
			printf(" Error al ejecutar el comando PONG.\n");
		else
			printf(" Ejecucion con exito del comando PONG.\n");
		break;

	case ERR_CANNOTSENDTOCHAN:
		printf(" Entro en el case ERR_CANNOTSENDTOCHAN.\n");

		if (analizarComandoErrCannotSendToChan(comando) == ERR)
			printf(" Error al ejecutar el comando ERR_CANNOTSENDTOCHAN.\n");
		else
			printf(" Ejecucion con exito del comando ERR_CANNOTSENDTOCHAN.\n");
		break;
	/*******************************************************************************/
	/*					FIN DE PARSEO DE MENSAJES DE RESPUESTA 					   */
	/*******************************************************************************/

	case OPER:
		printf("Entro en el case OPER\n");
		break;

	case MODE:
		printf("Entro en el case MODE\n");
		if (analizarComandoMode(comando) == ERR)
			printf(" Error al ejecutar el comando MODE\n");
		else
			printf(" Ejecucion con exito del comando MODE\n");
		break;

	case SERVICE:
		printf("Entro en el case SERVICE\n");
		break;

	case QUIT:
		printf(" Entro en el case QUIT\n");

		if (analizarComandoQuit(comando) == ERR)
			printf(" Error al ejecutar el comando QUIT\n");
		else
			printf(" Ejecucion con exito del comando QUIT\n");
		break;

	case SQUIT:
		printf("Entro en el case SQUIT\n");
		break;

	case JOIN:
		printf(" Entro en el case JOIN\n");

		if (analizarComandoJoin(comando) == ERR)
			printf(" Error al ejecutar el comando JOIN\n");
		else
			printf(" Ejecucion con exito del comando JOIN\n");
		break;

	case PART:
		printf(" Entro en el case PART\n");

		if (analizarComandoPart(comando) == ERR)
			printf(" Error al ejecutar el comando PART\n");
		else
			printf(" Ejecucion con exito del comando PART\n");
		break;

	case TOPIC:
		printf(" Entro en el case TOPIC\n");

		if (analizarComandoTopic(comando) == ERR)
			printf(" Error al ejecutar el comando TOPIC\n");
		else
			printf(" Ejecucion con exito del comando TOPIC\n");
		break;

	case NAMES:
		printf(" Entro en el case NAMES\n");

		if (analizarComandoNames(comando) == ERR)
			printf(" Error al ejecutar el comando NAMES\n");
		else
			printf(" Ejecucion con exito del comando NAMES\n");
		break;

	case LIST:
		printf(" Entro en el case LIST\n");

		if (analizarComandoList(comando) == ERR)
			printf(" Error al ejecutar el comando LIST\n");
		else
			printf(" Ejecucion con exito del comando LIST\n");
		break;

	case INVITE:
		printf("Entro en el case INVITE\n");
		break;

	case KICK:
		printf(" Entro en el case KICK\n");

		if (analizarComandoKick(comando) == ERR)
			printf(" Error al ejecutar el comando KICK\n");
		else
			printf(" Ejecucion con exito del comando KICK\n");
		break;

	case PRIVMSG:
		printf(" Entro en el case PRIVMSG\n");

		if (analizarComandoPrivMsg(comando) == ERR)
			printf(" Error al ejecutar el comando PRIVMSG\n");
		else
			printf(" Ejecucion con exito del comando PRIVMSG\n");
		break;

	case NOTICE:
		printf("Entro en el case NOTICE\n");
		break;

	case MOTD:
		printf(" Entro en el case MOTD\n");

		if (analizarComandoMotd(comando) == ERR)
			printf(" Error al ejecutar el comando MOTD\n");
		else
			printf(" Ejecucion con exito del comando MOTD\n");
		break;

	case LUSERS:
		printf("Entro en el case LUSERS\n");
		break;

	case VERSION:
		printf("Entro en el case VERSION\n");
		break;

	case STATS:
		printf("Entro en el case STATS\n");
		break;

	case LINKS:
		printf("Entro en el case LINKS\n");
		break;

	case TIME:
		printf("Entro en el case TIME\n");
		break;

	case CONNECT:
		printf("Entro en el case CONNECT\n");
		break;

	case TRACE:
		printf("Entro en el case TRACE\n");
		break;

	case ADMIN:
		printf("Entro en el case ADMIN\n");
		break;

	case INFO:
		printf("Entro en el case INFO\n");
		break;

	case SERVLIST:
		printf("Entro en el case SERVLIST\n");
		break;

	case SQUERY:
		printf("Entro en el case SQUERY\n");
		break;

	case WHO:
		printf("Entro en el case WHO\n");
		break;

	case WHOIS:
		printf(" Entro en el case WHOIS\n");

		if (analizarComandoWhoIs(comando) == ERR)
			printf(" Error al ejecutar el comando WHOIS\n");
		else
			printf(" Ejecucion con exito del comando WHOIS\n");
		break;

	case WHOWAS:
		printf("Entro en el case WHOWAS\n");
		break;

	case KILL:
		printf("Entro en el case KILL\n");
		break;

	case PING:
		printf(" Entro en el case PING\n");

		if (analizarComandoPing(comando) == ERR)
			printf(" Error al ejecutar el comando PING\n");
		else
			printf(" Ejecucion con exito del comando PING\n");
		break;

	case ERROR:
		printf("Entro en el case ERROR\n");
		break;

	case AWAY:
		printf(" Entro en el case AWAY\n");

		if (analizarComandoAway(comando) == ERR)
			printf(" Error al ejecutar el comando AWAY\n");
		else
			printf(" Ejecucion con exito del comando AWAY\n");
		break;

	case REHASH:
		printf("Entro en el case REHASH\n");
		break;

	case DIE:
		printf("Entro en el case DIE\n");
		break;

	case RESTART:
		printf("Entro en el case RESTART\n");
		break;

	case SUMMON:
		printf("Entro en el case SUMMON\n");
		break;

	case USERS:
		printf("Entro en el case USERS\n");
		break;

	case WALLOPS:
		printf("Entro en el case WALLOPS\n");
		break;

	case USERHOST:
		printf("Entro en el case USERHOST\n");
		break;

	case ISON:
		printf("Entro en el case ISON\n");
		break;

	case HELP:
		printf("Entro en el case HELP\n");
		break;

	case RULES:
		printf("Entro en el case RULES\n");
		break;

	case SERVER:
		printf("Entro en el case SERVER\n");
		break;

	case ENCAP:
		printf("Entro en el case ENCAP\n");
		break;

	case CNOTICE:
		printf("Entro en el case CNOTICE\n");
		break;

	case CPRIVMSG:
		printf("Entro en el case CPRIVMSG\n");
		break;

	case NAMESX:
		printf("Entro en el case NAMESX\n");
		break;

	case SILENCE:
		printf("Entro en el case SILENCE\n");
		break;

	case UHNAMES:
		printf("Entro en el case UHNAMES\n");
		break;

	case WATCH:
		printf("Entro en el case WATCH\n");
		break;

	case KNOCK:
		printf("Entro en el case KNOCK\n");
		break;

	case USERIP:
		printf("Entro en el case USERIP\n");
		break;

	case SETNAME:
		printf("Entro en el case SETNAME\n");
		break;

	default:
		printf(" Entro en el case DEFAULT\n");

		if (analizarComandoDesconocido(comando) == ERR)
			printf(" Error al ejecutar el comando DESCONOCIDO\n");
		else
			printf(" Ejecucion con exito del comando DESCONOCIDO\n");
	}
	return OK;
}