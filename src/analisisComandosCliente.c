#include "../includes/analisisComandosCliente.h"

extern int socket_id;


/**
 * \defgroup AnalisisComandosCliente Funciones de parseo de mensajes del usuario
 *
 */
/**
 * \addtogroup AnalisisComandosCliente
 * Funcion de parseo de los mensajes de Usuario, seran llamadas las fucniones de
 * comandos cliente para el analisis de los mismos
 *
 * <hr>
 */


/**
 * @brief Analizar Comando Cliente
 *
 * @synopsis
 *
 * int analizarComandosCliente(char * command)
 * @description
 *
 * Funcion que analiza un comando recibido desde la linea de texto del usuario
 *
 * @return
 * Esta funcion devuelve un entero que representa un error o una correcta ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandosCliente(char * command){

	switch (IRCUser_CommandQuery(command)) {

	case UJOIN:
		if (analizaUJoinCliente(command) == OK) {
			printf("(UJOIN) Ok\n");
		} else {
			printf("(UJOIN) Error\n");
		}
		break;

	case UNICK:
		if (analizaUNickCliente(command) == OK) {
			printf("(UNICK) Ok\n");
		} else {
			printf("(UNICK) Error\n");
		}
		break;

	case UNAMES:
		if (analizaUNamesCliente(command) == OK) {
			printf("(UNAMES) Ok\n");
		} else {
			printf("(UNAMES) Error\n");
		}
		break;

	case UPART:
		if (analizaUPartCliente(command) == OK) {
			printf("(UPART) Ok\n");
		} else {
			printf("(UPART) Error\n");
		}
		break;

	case ULEAVE:
		if (analizaULeaveCliente(command) == OK) {
			printf("(ULEAVE) Ok\n");
		} else {
			printf("(ULEAVE) Error\n");
		}
		break;

	case UQUIT:
		if (analizaUQuitCliente(command) == OK) {
			printf("(UQUIT) Ok\n");
		} else {
			printf("(UQUIT) Error\n");
		}
		break;

	case UMOTD:
		if (analizaUMotdCliente(command) == OK) {
			printf("(UMOTD) Ok\n");
		} else {
			printf("(UMOTD) Error\n");
		}
		break;

	case UAWAY:
		if (analizaUAwayCliente(command) == OK) {
			printf("(UAWAY) Ok\n");
		} else {
			printf("(UAWAY) Error\n");
		}
		break;	

	case UWHOIS:
		if (analizaUWhoIsCliente(command) == OK) {
			printf("(UWHOIS) Ok\n");
		} else {
			printf("(UWHOIS) Error\n");
		}
		break;
	case UNOTICE:
		if (analizaUNoticeCliente(command) == OK) {
			printf("(UNOTICE) Ok\n");
		} else {
			printf("(UNOTICE) Error\n");
		}
		break; 

	case UWHO:
		if (analizaUWhoCliente(command) == OK) {
			printf("(UWHO) Ok\n");
		} else {
			printf("(UWHO) Error\n");
		}
		break; 
	case UINVITE:
		if (analizaUInviteCliente(command) == OK) {
			printf("(UINVITE) Ok\n");
		} else {
			printf("(UINVITE) Error\n");
		}
		break;

	case UKICK:
		if (analizaUKickCliente(command) == OK) {
			printf("(UKICK) Ok\n");
		} else {
			printf("(UKICK) Error\n");
		}
		break;

	case UTOPIC:
		if (analizaUTopicCliente(command) == OK) {
			printf("(UTOPIC) Ok\n");
		} else {
			printf("(UTOPIC) Error\n");
		}
		break;

	case ULIST:
		if (analizaUListCliente(command) == OK) {
			printf("(ULIST) Ok\n");
		} else {
			printf("(ULIST) Error\n");
		}
		break;
	case UCYCLE:
		if (analizaUCycleCliente(command) == OK) {
			printf("(UCYCLE) Ok\n");
		} else {
			printf("(UCYCLE) Error\n");
		}
		break;

	case UMSG:
		if (analizaUMsgCliente(command) == OK) {
			printf("(UMSG) Ok\n");
		} else {
			printf("(UMSG) Error\n");
		}
		break;
	default:

		analizarTextoUsuario(command);
	}

	return OK;






}