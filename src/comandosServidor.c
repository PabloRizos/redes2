#include "../includes/comandosServidor.h"

extern int socket_id;
extern clock_t tiempo_ping;
int joinFlag = 0;
/**
 * \defgroup comandosServidor Funciones de atencion al hilo del servidor
 *
 */
/**
 * \addtogroup ComandosServidor
 * Funciones que van a ser llamadas cuando se crea el hilo de recepcion del
 * servidor.
 *
 * <hr>
 */

//int enviaMsgUsuarios(char * msg, char **listusers, long * numerousuarios){
//
//	int i=0, j=0, socket_aux=0;
//
//	syslog(LOG_INFO, "Entrando en el bucle de envio (enviaMsgUsuarios)");
//	for(i=0; i<*numerousuarios; i++){
//		for(j=0; j<=usuConectados.conexiones; j++){
//			if(!strcmp(listusers[i], usuConectados.conectados[j].nick)){
//				socket_aux = usuConectados.conectados[j].socketUsu;
//				envioServidor(socket_aux, msg);
//			}
//		}
//	}
//	return 0;
//}
//
//
//int enviaMsgOneUser(char * msg, char * nickUsuario){
//
//	int i=0, socket_aux=0;
//
//	syslog(LOG_INFO, "Entrando en el bucle de envio (enviaMsgUsuarios)");
//
//	for (i=0; i<=usuConectados.conexiones; i++){
//		if(!strcmp(nickUsuario, usuConectados.conectados[i].nick)){
//			socket_aux = usuConectados.conectados[i].socketUsu;
//			envioServidor(socket_aux, msg);
//			return 0;
//		}
//	}
//
//	return -1;
//}
//
///**
//* Funcion auxiliar para la inicializacion del usuario actual.
//* @param nick
//* @param usuActual El usuario con nick y socket que envia el comando
//* @return ERR u OK
//*/
//int initUserByNick(char * nick, pUsusocket nickActual){
//
//		if(nick==NULL)
//			return ERR;
//		if(strlen(nick)==0)
//			return ERR;
//		syslog(LOG_INFO, "----------Estoy dentro de la funcion initUsuario. NICK: %s", nick);
//		/* Si el nick a cambiar es el actual */
//		if (strcmp(nickActual->nick, nick) == 0) {
//			return OK;
//		}
//		syslog(LOG_INFO, "----------Estoy dentro de la funcion initUsuario(strcpy)");
//
//		strcpy(nickActual->nick, nick);
//		return OK;
//}
int analizarComandoPass(char* comando) { return OK; }


/**
 * @brief analizarComandoNick
 *
 * @synopsis
 *
 * int analizarComandoNick(char * comando)
 * @description
 *
 * Funcion que analiza un comando de nick del servidor
 *
 * @return
 * Devuelve ERR si el servidor devuelve el nick mal, u OK en caso de exito
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoNick(char* comando) {

	char* prefijo = NULL, * nick = NULL, * msg = NULL, mensaje[TAM_MAX_MENSAJE];
	char* nick1 = NULL, *user = NULL, *realname = NULL, *pass = NULL, *server = NULL;
	int port = 0, ssl = 0;
	mensaje[0] = 0;

	IRCInterface_GetMyUserInfoThread(&nick1, &user, &realname, &pass, &server, &port, &ssl);
	if (IRCParse_Nick(comando, &prefijo, &nick, &msg) != IRC_OK) {
		IRCInterface_WriteSystemThread(NULL, "Error al recibir el nick del servidor");
		return ERR;
	}

	if (strcmp(nick1, msg)) {
		sprintf(mensaje, "Ahora es usted conocido como %s", msg);
		IRCInterface_WriteSystemThread(NULL, mensaje);
		IRCInterface_ChangeNickThread(nick1, msg);
	}
	multiple_free(9, prefijo, nick, msg, nick, nick1, user, realname, pass, server);
	return OK;

}

/**
 * @brief analizarComandoERRNickNameInUse
 *
 * @synopsis
 *
 * int analizarComandoERRNickNameInUse(char * comando)
 * @description
 *
 * Funcion que analiza un comando de ErrNickNameInUse del servidor
 *
 * @return
 * Devuelve ERR si el servidor devuelve el nick mal, u OK en caso de exito
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoERRNickNameInUse(char* comando) {

	char* nick1 = NULL, *user = NULL, *realname = NULL, *pass = NULL, *server = NULL, *mensaje2 = NULL;
	char* type = NULL, **parameters = NULL, *msg = NULL, *prefix = NULL, mensaje[TAM_MAX_MENSAJE];
	int port = 0, ssl = 0, numparameters = 0;

	IRCInterface_GetMyUserInfoThread(&nick1, &user, &realname, &pass, &server, &port, &ssl);

	IRCParse_GeneralCommand(comando, &prefix, &type, &parameters, &numparameters, &msg);
	IRCInterface_WriteSystemThread(NULL, msg);

	sprintf(mensaje, "%s_", parameters[1]);
	IRCMsg_Nick(&mensaje2, NULL, mensaje, NULL);
	enviar(socket_id, mensaje2);
	return OK;
}
int analizarComandoUser(char* comando) { return OK; }

/**
 * @brief analizarComandoJoin
 *
 * @synopsis
 *
 * int analizarComandoJoin(char * comando)
 * @description
 *
 * Funcion que analiza un comando JOIN por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoJoin(char* comando) {

	char* join = NULL, *prefix = NULL, *channel = NULL, *key = NULL, *msg = NULL, * mode = NULL;
	char* nick1 = NULL, *user = NULL, *realname = NULL, *pass = NULL, *server = NULL, *msgWho = NULL;
	int port = 0, ssl = 0;
	long dev = 0;

	IRCInterface_GetMyUserInfoThread(&nick1, &user, &realname, &pass, &server, &port, &ssl);

	if ((dev = IRCParse_Join(comando, &prefix, &channel, &key, &msg)) != IRC_OK) {
		printf("(SJOIN)Error al recibir el join del servidor");
		IRCInterface_WriteSystemThread(NULL, "Error al recibir el join del servidor");
		return ERR;
	}
	printf("channel: %s\n", msg);
	IRCInterface_AddNewChannelThread(msg, 0);

	printf("Creo el mensaje de modo\n");

	if (IRCMsg_Mode(&mode, NULL, msg, "+o", nick1) != IRC_OK) {
		printf("Error al crear el mensaje de MODE");
		IRCInterface_WriteSystemThread(NULL, "(SJOIN)Error al crear el mensaje de MODE");
		return ERR;
	}
	printf("Mensaje de modo despues del join: %s", mode);
	IRCInterface_PlaneRegisterOutMessageThread(mode);
	enviar(socket_id, mode);

	printf("Creo el mensaje de who\n");
	if (IRCMsg_Who(&msgWho, NULL, msg, NULL) != IRC_OK) {
		printf("Error al crear el mensaje de WHO");
		IRCInterface_WriteSystemThread(NULL, "(SJOIN)Error al crear el mensaje de WHO");
		return ERR;
	}
	printf("Mensaje de who despues del join: %s", msgWho);
	IRCInterface_PlaneRegisterOutMessageThread(msgWho);
	enviar(socket_id, msgWho);


	printf("Antes del mfree\n");
	multiple_free(11, join, prefix, channel, key, msg, mode, nick1, user, realname, pass, server);
	joinFlag = 1;
	return OK;
}

/**
 * @brief analizarComandoNameRply
 *
 * @synopsis
 *
 * int analizarComandoNameReply(char * comando)
 * @description
 *
 * Funcion que analiza un comando NameRply por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoNameReply(char* comando) {

	char* nick = NULL, *prefix = NULL, *channel = NULL, *type = NULL, *msg = NULL;

	IRCParse_RplNamReply(comando, &prefix, &nick, &type, &channel, &msg);

	multiple_free(5, prefix, nick, msg, channel, type);
	return OK;
}

/**
 * @brief analizarComandoEnOfNames
 *
 * @synopsis
 *
 * int analizarComandoEndNames(char * comando)
 * @description
 *
 * Funcion que analiza un comando EndOfNames por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoEndNames(char* comando) {

	char* nick = NULL, *prefix = NULL, *channel = NULL, *msg = NULL;
	IRCParse_RplEndOfNames(comando, &prefix, &nick, &channel, &msg);
	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(4, prefix, nick, msg, channel);
	return OK;
}

/**
 * @brief analizarComandoWho
 *
 * @synopsis
 *
 * int analizarComandoWho(char * comando)
 * @description
 *
 * Funcion que analiza un comando Who por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoWho(char* comando) {

	char* nick = NULL, *prefix = NULL, *channel = NULL, *msg = NULL, *user = NULL;
	char* host = NULL, *server = NULL, *nick2 = NULL, *type = NULL, *realname = NULL;
	char cadena[TAM_MAX_MENSAJE];
	int hopcount = 0, i = 0;
	nickstate ns = NONE;

	cadena[0] = 0;
	IRCParse_RplWhoReply(comando, &prefix, &nick, &channel, &user, &host, &server, &nick2, &type, &msg, &hopcount, &realname);

	/** Si el usuario es operador, actualizamos su nickstate */
	if (!strcmp(type, "H@") || !strcmp(type, "G@"))
		ns = OPERATOR;
	printf("Antes de comprobacion de channel\n");
	if (strcmp(channel, "*")) {
		while (comando[i] != '#')
			i++;
		strcpy(cadena, comando + i);
		IRCInterface_WriteSystemThread(NULL, cadena);
		IRCInterface_AddNickChannelThread(channel, nick2, user, realname, host, ns);
		printf("AddNickChannel\n");
		printf("%s\n", type);
	} else {
		while (comando[i] != '*')
			i++;
		strcpy(cadena, comando + i);
		IRCInterface_WriteSystemThread(NULL, cadena);
	}
	multiple_free(9, nick, prefix, channel, msg, user, host, server, nick2, realname);
	return OK;
}
/**
 * @brief analizarComandoEnOfWho
 *
 * @synopsis
 *
 * int analizarComandoEndOfWho(char * comando)
 * @description
 *
 * Funcion que analiza un comando EndOfWho por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoEndOfWho(char* comando) {

	char* nick = NULL, *prefix = NULL, *name = NULL, *msg = NULL;
	IRCParse_RplEndOfWho(comando, &prefix, &nick, &name, &msg);
	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(4, prefix, nick, msg, name);
	return OK;
}
/**
 * @brief analizarComandoWelcome
 *
 * @synopsis
 *
 * int analizarComandoWelcome(char * comando)
 * @description
 *
 * Funcion que analiza un comando Welcome por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoWelcome(char* comando) {

	char* prefix = NULL, *nick = NULL, *msg = NULL;

	IRCParse_RplWelcome(comando, &prefix, &nick, &msg);
	multiple_free(3, prefix, nick, msg);
	return OK;
}
/**
 * @brief analizarComandoMotd
 *
 * @synopsis
 *
 * int analizarComandoMotd(char * comando)
 * @description
 *
 * Funcion que analiza un comando Motd por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoMotd(char* comando) {

	char* prefix = NULL, *nick = NULL, *msg = NULL;

	IRCParse_RplMotd(comando, &prefix, &nick, &msg);
	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(3, prefix, nick, msg);
	return OK;

}
/**
 * @brief analizarComandoYourHost
 *
 * @synopsis
 *
 * int analizarComandoYourHost(char * command)
 * @description
 *
 * Funcion que analiza un comando YourHost por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoYourHost(char* command) {

	char* prefix = NULL, *nick = NULL, *msg = NULL, *servername = NULL, *versionname = NULL;
	IRCParse_RplYourHost(command, &prefix, &nick, &msg, &servername, &versionname);
	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(5, prefix, nick, msg, servername, versionname);
	return OK;

}
/**
 * @brief analizarComandoCreated
 *
 * @synopsis
 *
 * int analizarComandoCreated(char * command)
 * @description
 *
 * Funcion que analiza un comando Created por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoCreated(char* command) {

	char* prefix = NULL, *nick = NULL, *msg = NULL, *timedate = NULL;
	IRCParse_RplCreated(command, &prefix, &nick, &timedate, &msg);

	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(4, prefix, nick, msg, timedate);

	return OK;

}
/**
 * @brief analizarComandoMyInfo
 *
 * @synopsis
 *
 * int analizarComandoMyInfo(char * command)
 * @description
 *
 * Funcion que analiza un comando MyInfo por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoMyInfo(char* command) {

	char* prefix = NULL, *nick = NULL, *servername = NULL, *version = NULL;
	char* availableusermodes = NULL, *availablechannelmodes = NULL, *addedg = NULL;
	IRCParse_RplMyInfo(command, &prefix, &nick, &servername, &version, &availableusermodes, &availablechannelmodes, &addedg);

	IRCInterface_WriteSystemThread(NULL, availableusermodes);

	multiple_free(7, prefix, nick, servername, version, availableusermodes, availablechannelmodes, addedg);
	return OK;
}
/**
 * @brief analizarComandoBounce
 *
 * @synopsis
 *
 * int analizarComandoBounce(char * command)
 * @description
 *
 * Funcion que analiza un comando EndOfNames por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoBounce(char* command) {

	char* prefix = NULL, *nick = NULL, *servername = NULL, *msg = NULL;
	int port = 0;
	IRCParse_RplBounce(command, &prefix, &nick,  &servername,  &port, &msg);

	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(4, prefix, nick, servername, msg);
	return OK;
}
/**
 * @brief analizarComandoLusers
 *
 * @synopsis
 *
 * int analizarComandoLusersClient(char * comando)
 * @description
 *
 * Funcion que analiza un comando LUsers por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoLusersClient(char* comando) {

	char* prefix = NULL, *nick = NULL, *msg = NULL;
	int ninvisibles = 0, nusers = 0, nservers = 0;
	IRCParse_RplLuserClient(comando, &prefix, &nick, &msg, &nusers, &ninvisibles, &nservers);

	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(3, prefix, nick, msg);
	return OK;
}
/**
 * @brief analizarComandoLusers
 *
 * @synopsis
 *
 * int analizarComandoLusers(char * comando)
 * @description
 *
 * Funcion que analiza un comando Lusers por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoLusers(char* comando) {

	char* prefix = NULL, *nick = NULL, *msg = NULL;
	int nclients = 0, nservers = 0;
	IRCParse_RplLuserMe(comando, &prefix, &nick, &msg, &nclients, &nservers);

	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(3, prefix, nick, msg);
	return OK;
}
/**
 * @brief analizarComandoLusersChannel
 *
 * @synopsis
 *
 * int analizarComandoLusersChannels(char * comando)
 * @description
 *
 * Funcion que analiza un comando LusersChannel por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoLusersChannels(char* comando) {

	char* prefix = NULL, *nick = NULL, *msg = NULL;
	int nchannels = 0;
	IRCParse_RplLuserChannels(comando, &prefix, &nick, &nchannels, &msg);

	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(3, prefix, nick, msg);
	return OK;
}
/**
 * @brief analizarComandoEndOfMotd
 *
 * @synopsis
 *
 * int analizarComandoEndMotd(char * comando)
 * @description
 *
 * Funcion que analiza un comando EndOfMotd por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoEndMotd(char* comando) {

	char* prefix = NULL, *nick = NULL, *msg = NULL;
	IRCParse_RplEndOfMotd(comando, &prefix, &nick, &msg);

	IRCInterface_WriteSystemThread(NULL, msg);
	multiple_free(3, prefix, nick, msg);
	return OK;
}
/**
 * @brief analizarComandoList
 *
 * @synopsis
 *
 * int analizarComandoList(char * comando)
 * @description
 *
 * Funcion que analiza un comando List por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoList(char* comando) { return OK; }
int analizarComandoWhoIs(char* comando) { return OK; }
int analizarComandoNames(char* comando) { return OK; }
/**
 * @brief analizarComandoKick
 *
 * @synopsis
 *
 * int analizarComandoKick(char * comando)
 * @description
 *
 * Funcion que analiza un comando KICK por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoKick(char* comando) {

	char* prefix = NULL, *channel = NULL, *user = NULL, *comment = NULL, *nick = NULL;
	char* user2 = NULL, *host = NULL, *server = NULL, *real = NULL, *pass = NULL, *nick2 = NULL;
	char mensaje[TAM_MAX_MENSAJE];
	int port = 0, ssl = 0;
	mensaje[0] = 0;

	IRCParse_Kick(comando, &prefix, &channel, &user, &comment);
	IRCParse_ComplexUser(prefix, &nick, &user2, &host, &server);

	IRCInterface_DeleteNickChannelThread(channel, user);
	sprintf(mensaje, "%s ha echado a %s de %s (%s)", nick, user, channel, comment);
	IRCInterface_WriteChannelThread(channel, "*", mensaje);

	free(server);
	IRCInterface_GetMyUserInfoThread(&nick2, &user2, &real, &pass, &server, &port, &ssl);
	if (!strcmp(nick, user)) {
		IRCInterface_RemoveChannelThread(channel);
		sprintf(mensaje, "Usted ha sido expulsado de %s por %s (%s)", channel, nick, comment);
		IRCInterface_WriteSystemThread(NULL, mensaje);
	}
	multiple_free(9, prefix, channel, user, comment, nick, user, host, server, real, pass, user2);
	return OK;



}
/**
 * @brief analizarComandoPrvMsg
 *
 * @synopsis
 *
 * int analizarComandoPrivMsg(char * comando)
 * @description
 *
 * Funcion que analiza un comando PrvMsg por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoPrivMsg(char* comando) {

	char* nick = NULL, *user = NULL, *server = NULL, *host = NULL, *realname = NULL;
	char* prefix = NULL, *msgtarget = NULL, *msg = NULL, *pass = NULL, *nickname = NULL;
	char* username = NULL;
	int port = 0, ssl = 0;

	IRCInterface_GetMyUserInfoThread(&nick, &user, &realname, &pass, &server, &port, &ssl);
	IRCParse_Privmsg(comando, &prefix, &msgtarget, &msg);
	IRCParse_ComplexUser(prefix, &nickname, &username, &host, &server);
	if (IRCInterface_QueryChannelExistThread(msgtarget)) {
		IRCInterface_WriteChannelThread(msgtarget, nickname, msg);
	} else if (!strcmp(msgtarget, nick))
		IRCInterface_WriteChannel(nick, nickname, msg);

	multiple_free(12, prefix, msgtarget, msg, nick, user, server, realname, host, pass, username, nickname, host);
	return OK;
}

/**
 * @brief analizarComandoTopic
 *
 * @synopsis
 *
 * int analizarComandoTopic(char * comando)
 * @description
 *
 * Funcion que analiza un comando TOPIC por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoTopic(char* comando) {

	char* prefix = NULL, *channel = NULL, *topic = NULL, *nick = NULL, *user = NULL;
	char* host = NULL, *server = NULL;
	char mensaje[TAM_MAX_MENSAJE];
	mensaje[0] = 0;

	IRCParse_Topic(comando, &prefix, &channel, &topic);
	IRCParse_ComplexUser(prefix, &nick, &user, &host, &server);
	sprintf(mensaje, "%s ha cambiado el topico a: %s", nick, topic);
	IRCInterface_WriteChannel(channel, "*", mensaje);

	multiple_free(7, prefix, channel, topic, nick, user, host, server);
	return OK;
}

int analizarComandoAway(char* comando) { return OK; }

/**
 * @brief analizarComandoPart
 *
 * @synopsis
 *
 * int analizarComandoPart(char * comando)
 * @description
 *
 * Funcion que analiza un comando Part por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoPart(char* comando) {

	char* prefix = NULL, *channel = NULL, *msg = NULL, *nick = NULL, *user = NULL;
	char* host = NULL, *server = NULL, mensaje[TAM_MAX_MENSAJE];

	mensaje[0] = 0;
	IRCParse_Part(comando, &prefix, &channel, &msg);
	IRCParse_ComplexUser(prefix, &nick, &user, &host, &server);
	IRCInterface_DeleteNickChannelThread(channel, nick);

	sprintf(mensaje, "%s ha abandonado el canal", nick);
	IRCInterface_WriteChannelThread(channel, nick, mensaje);
	multiple_free(7, prefix, channel, msg, nick, user, host, server);
	return OK;


}

int analizarComandoPing(char* comando) { return OK; }
int analizarComandoQuit(char* comando) { return OK; }

/**
 * @brief analizarComandoMode
 *
 * @synopsis
 *
 * int analizarComandoMode(char * comando)
 * @description
 *
 * Funcion que analiza un comando MODE por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR segun el flujo de ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoMode(char* comando) {

	char* prefix = NULL, *channel = NULL, *mode = NULL, *user = NULL;
	char* nick = NULL, * aux1 = NULL, *aux2 = NULL, *aux3 = NULL;
	char mensaje[TAM_MAX_MENSAJE];
	nickstate ns = NONE;

	mensaje[0] = 0;

	if (IRCParse_Mode(comando, &prefix, &channel, &mode, &user) != IRC_OK) {
		printf("Error al recibir el mode del servidor");
		IRCInterface_WriteSystemThread(NULL, "(SMODE) Error al recibir el mode del servidor");
		return ERR;
	}
	IRCParse_ComplexUser(prefix, &nick, &aux1, &aux2, &aux3);
	printf("MODE: %s\n", mode);

    /** Comprobacion de los modos del canal */
	if (!strcmp(mode, "+o")){
		ns = OPERATOR;
        printf("USER MODE: %s\n", user);
        IRCInterface_ChangeNickStateChannelThread(channel, user, ns);
        sprintf(mensaje, "%s da OP a %s", nick, user);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if (!strcmp(mode, "-o")){
        ns = NONE;
        printf("USER MODE: %s\n", user);
        IRCInterface_ChangeNickStateChannelThread(channel, user, ns);
        sprintf(mensaje, "%s quita OP a %s", nick, user);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
	else if (!strcmp(mode, "+b")) {
		sprintf(mensaje, "%s banea a %s", nick, user);
		IRCInterface_WriteChannelThread(channel, "*", mensaje);

	} else if(!strcmp(mode, "+l")){
        sprintf(mensaje, "%s establece el limite del canal a %s", nick, user);
		IRCInterface_WriteChannelThread(channel, "*", mensaje);

    } else if(!strcmp(mode, "-l")){
        sprintf(mensaje, "%s ha eliminado el limite del canal", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "+k")){
        sprintf(mensaje, "%s establece la contraseña del canal como %s", nick, user);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "-k")){
        sprintf(mensaje, "%s quita la contraseña del canal", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "+t")){
        sprintf(mensaje, "%s ha establecido la proteccion de topic", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "-t")){
        sprintf(mensaje, "%s ha eliminado la proteccion de topic", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "+n")){
        sprintf(mensaje, "%s ha permitido mensajes externos", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "-n")){
        sprintf(mensaje, "%s ha bloqueado mensajes externos", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "+s")){
        sprintf(mensaje, "%s ha establecido el canal como secreto", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "-s")){
        sprintf(mensaje, "%s ha establecido el canal como publico", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "+i")){
        sprintf(mensaje, "%s ha establecido el canal como solo por invitacion", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "-i")){
        sprintf(mensaje, "%s ha establecido el canal como abierto", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "+m")){
        sprintf(mensaje, "%s ha establecido el canal como moderado", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
    else if(!strcmp(mode, "-m")){
        sprintf(mensaje, "%s ha establecido el canal como no moderado", nick);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }else{
        sprintf(mensaje, "%s establece modo %s %s", nick, mode, channel);
        IRCInterface_WriteChannelThread(channel, "*", mensaje);
    }
	multiple_free(8, prefix, channel, mode, user, nick, aux1, aux2, aux3);

	return OK;
}

/**
 * @brief analizarComandoErrCannotSendToChan
 *
 * @synopsis
 *
 * int analizarComandoErrCannotSendToChan(char * comando)
 * @description
 *
 * Funcion que analiza un comando ErrCannotSendToChan por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoErrCannotSendToChan(char * comando){

    char mensaje[TAM_MAX_MENSAJE], * prefix=NULL, * nick=NULL, *channel=NULL, *msg=NULL;

    mensaje[0]=0;
    IRCParse_ErrCanNotSendToChan (comando, &prefix, &nick, &channel, &msg);
    
    sprintf(mensaje, "%s :%s", channel, msg);

    if(!IRCInterface_QueryChannelExistThread(channel))
        IRCInterface_WriteSystemThread(NULL, mensaje);
    else
        IRCInterface_WriteChannelThread(channel, "*", mensaje);

    multiple_free(4, prefix, nick, channel, msg);
    return OK;
}
/**
 * @brief analizarComandoPong
 *
 * @synopsis
 *
 * int analizarComandoPong(char * comando)
 * @description
 *
 * Funcion que analiza un comando Pong por el cliente
 *
 * @return
 * La funcion devuelve OK o ERR dependiendo del flujo
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoPong(char* comando) {

	tiempo_ping = 0;
	return OK;
}
/**
 * @brief analizarComandoDesconocido
 *
 * @synopsis
 *
 * int analizarComandoDesconocido(char * comando)
 * @description
 *
 * Funcion que analiza un comando desconocido por el cliente
 *
 * @return
 * La funcion siempre devolvera OK debido que controla todo el flujo posible
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int analizarComandoDesconocido(char* comando) {
	printf("Default command");
	printf("%s", comando);
	if (!strcmp(comando, "CAP")) {
		enviar(socket_id, "CAP END");
	} else {
		printf("%s\n", comando);
	}
	return OK;
}