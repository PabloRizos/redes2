#include "../includes/sockets_cliente.h"


int socket_id=0;
/**
 * \defgroup SocketsCliente Funciones de conexion, desconexion y envio y recepcion
 * de mensajes a traves de sockets
 *
 */
/**
 * \addtogroup SocketsCliente
 * Funciones que sirven para el envio y recepcion de mensajes, tanto el envio desde
 * el usuario como la recepcion por parte de los mensajes del servidor.
 * Existe una funcion para cada accion basica de conexion.
 * <hr>
 */

/**
 * @brief Abrir Socket
 *
 * @synopsis
 *
 * int abrirSocket(int puerto, char * conexion)
 * @description
 *
 * Funcion que abre un socket para el envio y recepcion de mensajes
 *
 * @return
 * Esta funcion devuelve un entero que representa un error o una correcta ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int abrirSocket(int puerto, char * conexion){
	
	int optionValue = 1;
	struct sockaddr_in servidor;
  	struct hostent *he=NULL;
  	char ip[IP_MAX];

  	do{
	    he=gethostbyname(conexion);
    }while(he==NULL);

	ip[0] = 0;
    printf("Nombre del host: %s\n", he->h_name);
      
      /* muestra el nombre del nodo */
    hostname_to_ip(conexion, ip);
    printf("IP: %s\n", ip);
	/* Creamos el descriptor del socket a utilizar */
	printf("----------Creando socket de cliente----------");
	if ((socket_id = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		printf("----------ERROR CREANDO SOCKET");
		exit(EXIT_FAILURE);
	}
	printf("Socket: %d\n", socket_id);
	printf("Antes de rellenar estructura\n");

	/* Rellenamos la estructura descriptora de la conexion */
	servidor.sin_family = AF_INET; 							/* Familia del servidor: TCP/IP */
	servidor.sin_port = htons(puerto); 						/* Puerto de servidor echo */
	servidor.sin_addr = *((struct in_addr *)he->h_addr);	/* Direccion del servidor */
	bzero((void*)&(servidor.sin_zero), 8); 					/* Rellenamos con 0's dicho campo */

	printf("Despues del relleno\n");

	printf("Connect\n");
	/* Nos conectamos con el servidor con el socket creado*/
	printf("sock = %d \n", socket_id);
	if(connect(socket_id, (struct sockaddr *)&servidor, sizeof(struct sockaddr))==-1){ 
      /* llamada a connect() */
      printf("connect() error\n");
      exit(EXIT_FAILURE);
   }
   puts("AQUIIIIIII\n");
   
	return socket_id;
}
/**
 * @brief CerrarSocket
 *
 * @synopsis
 *
 * void cerrarSocket(int socket)
 * @description
 *
 * Funcion que cierra un socket y libera su respectivo descriptor
 *
 * @return
 * Esta funcion devuelve un entero que representa un error o una correcta ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
void cerrarSocket(int socket){

	printf("----------Cerrando socket del servidor----------");
	closelog();
	/* Cerramos el socket apagando su recepcion y envio */
	shutdown(socket, DOWN_SOCKET); 
	close(socket);

	return;
}

/**
 * @brief recibir
 *
 * @synopsis
 *
 * int recibir(int socket_id, char * mensaje)
 * @description
 *
 * Funcion que recibe un mensaje a traves de un socket dado
 *
 * @return
 * Esta funcion devuelve un entero que representa un error o una correcta ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int recibir(int socket_id, char * mensaje){
	
	int ret=0;

	if(mensaje==NULL || socket_id<0)
		return ERR;
	else{
		ret = recv(socket_id, mensaje, MAX_CADENA, 0);
		mensaje[strlen(mensaje)]= 0;
	}
	return ret;
}

/**
 * @brief enviar
 *
 * @synopsis
 *
 * int enviar(int socket_id, char* mensaje)
 * @description
 *
 * Funcion que envia un mensaje a traves de un socket dado
 *
 * @return
 * Esta funcion devuelve un entero que representa un error o una correcta ejecucion
 *
 * @author
 * Pablo Gomez Delgado y Daniel Santaella
 *
 *<hr>
*/
int enviar(int socket_id, char* mensaje){

	int ret=0;
	if(mensaje == NULL){
		printf("------Mensaje a enviar vacio");
		return ERR;
	}
	if(strlen(mensaje)> MAX_CADENA){
		printf("------Mensaje a enviar mayor que el maximo permitido");
		return ERR;
	}

	ret = send(socket_id, mensaje, strlen(mensaje), 0);
	return ret;
}
